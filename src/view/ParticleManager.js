import Phaser from 'phaser'

/*
 * This class handles creation and cleanup of Phaser.GameObjects.Image
 */
export default class ParticleManager extends Phaser.GameObjects.Container
{
  constructor(scene)
  {
    super(scene);

    this.rnd = Phaser.Math.RND;

    this.scene.add.existing(this);
  }

  randomPositiveNegative() {
   return this.rnd.sign();
	}

  randomRange( min, max ) {
		return this.rnd.realInRange(min, max);
	}

  randomParticleDest() {
		return this.randomRange( 1, 50 ) * this.randomPositiveNegative();
	}

  randomParticleRot() {
		return this.randomRange( 0, Math.PI * 2 ) * this.randomPositiveNegative();
	}

  r(n) {
		return n + this.randomParticleDest();
	}

  t(n = 0) {
		return n + this.randomParticleRot();
	}

  u() {
		return this.randomRange(0, 0.2);
	}

  starParticles( x, y, num = 12 )
	{
		for ( let i = 0; i < num; i++ )
		{
			let s = new Phaser.GameObjects.Image( this.scene, x, y, "star" );
      let startingScale = 3 / s.width;
      let endingScale = 15 / s.width;
			s.scale = startingScale;
			s.rotation = this.t();
			s.alpha = 0;
      this.scene.add.existing(s);
      this.scene.tweens.add({
        targets: s,
        alpha: 1,
        duration: 300,
        ease: 'Quad.easeInOut',
      });
      this.scene.tweens.add({
        targets: s,
        alpha: 0,
        duration: 300,
        delay: 300,
        ease: 'Quad.easeInOut',
      });
      this.scene.tweens.add({
        targets: s,
        scale: endingScale,
        x: this.r(s.x),
        y: this.r(s.y),
        rotation: this.t(),
        duration: 600,
        ease: 'Expo.easeOut',
        onComplete: () => this.remove(s, true)
      });
		}
	}

  smokeParticles( x, y, num = 5 )
	{
		for ( let i = 0; i < num; i++ )
		{
			let s = new Phaser.GameObjects.Image( this.scene, x, y, "smoke" );
      s.scale = 30 / s.width;
			s.alpha = 0;
			this.scene.add.existing(s);
      this.scene.tweens.add({
        targets: s,
        alpha: 1,
        duration: 200,
        ease: 'Quad.easeInOut',
      });
      this.scene.tweens.add({
        targets: s,
        alpha: 0,
        delay: 500,
        duration: 300,
        ease: 'Quad.easeInOut',
      });
      this.scene.tweens.add({
        targets: s,
        x: s.x + this.randomRange( 1, 50 ) * this.randomPositiveNegative(),
        y: s.y - this.randomRange( 10, 50 ),
        duration: 800,
        ease: 'Expo.easeOut',
        onComplete: () => this.remove(s, true)
      });
		}
	}

  steamParticles( x, y, num = 10 )
	{
		for ( let i = 0; i < num; i++ )
		{
			let s = new Phaser.GameObjects.Image( this.scene, x, y, "splash" );
			s.scale = 30 / s.width;
			s.alpha = 0;
			this.scene.add.existing(s);
      this.scene.tweens.add({
        targets: s,
        alpha: 1,
        duration: 500,
        ease: 'Quad.easeInOut',
      });
      this.scene.tweens.add({
        targets: s,
        alpha: 0,
        delay: 500,
        duration: 300,
        ease: 'Quad.easeInOut',
      });
      this.scene.tweens.add({
        targets: s,
        x: s.x + this.randomRange( 1, 50 ) * this.randomPositiveNegative(),
        y: s.y - this.randomRange( 0, 40 ),
        duration: 800,
        ease: 'Expo.easeOut',
        onComplete: () => this.remove(s, true)
      });
		}
	}

  freezeParticles( x, y, num = 10 )
	{
    let startingWidth = 15;

		for ( let i = 0; i < num; i++ )
		{
			let s = new Phaser.GameObjects.Image(
        this.scene,
        this.r(x),
        this.r(y),
        "snowflake"
      );
      let endingScale = 2 / s.width;
			s.scale = startingWidth / s.width;
			s.rotation = this.t();
			s.alpha = 0;
			this.scene.add.existing(s);

      this.scene.tweens.add({
        targets: s,
        alpha: 1,
        duration: 300,
        ease: 'Quad.easeOut',
      });
      this.scene.tweens.add({
        targets: s,
        alpha: 0,
        duration: 300,
        delay: 300,
        ease: 'Quad.easeIn',
      });
      this.scene.tweens.add({
        targets: s,
        scale: endingScale,
        x: x,
        y: y,
        rotation: this.t(),
        duration: 600,
        ease: 'Expo.easeIn',
        onComplete: () => this.remove(s, true)
      });
		}
  }

  activateParticles( x, y, num = 10 )
	{
		for( let i = 0; i < num; i++ )
		{
			let s = new Phaser.GameObjects.Image(
        this.scene,
        x + this.randomRange(0, 50) * this.randomPositiveNegative(),
        y + this.randomRange(0, 30) * this.randomPositiveNegative(),
        "glow"
      );
      s.scale = 20 / s.width;
			s.alpha = 0;
			this.scene.add.existing(s);

      this.scene.tweens.add({
        targets: s,
        alpha: 1,
        duration: 300,
        ease: 'Quad.easeOut',
      });
      this.scene.tweens.add({
        targets: s,
        alpha: 0,
        duration: 300,
        delay: 300,
        ease: 'Quad.easeIn',
      });
      this.scene.tweens.add({
        targets: s,
        scale: 0,
        x: x,
        y: y,
        duration: 600,
        ease: 'Expo.easeIn',
        onComplete: () => this.remove(s, true)
      });
		}
	}

  explosionParticles( x, y, num = 3 )
	{
		for( let i = 0; i < num; i++ )
		{
			let s = new Phaser.GameObjects.Image(
        this.scene,
        x + this.randomRange(0, 10) * this.randomPositiveNegative(),
        y + this.randomRange(0, 10) * this.randomPositiveNegative(),
        "explosion"
      );
      let endingScale = 120 / s.width;
      s.scale = 20 / s.width;
			s.alpha = 0;
			this.scene.add.existing(s);

			let d = Math.round( this.randomRange(0, 200) );
      //console.log( d );
      this.scene.tweens.add({
        targets: s,
        alpha: 0.8,
        duration: 150,
        delay: d,
        ease: 'Quad.easeOut',
      });
      this.scene.tweens.add({
        targets: s,
        alpha: 0,
        duration: 350,
        delay: 150+d,
        ease: 'Quad.easeIn',
      });
      this.scene.tweens.add({
        targets: s,
        x: s.x - this.randomRange(10,30) * this.randomPositiveNegative(),
        y: s.y - this.randomRange(0,20) * this.randomPositiveNegative(),
        scale: endingScale,
        duration: 500,
        delay: d,
        ease: 'Expo.easeOut',
        onComplete: () => this.remove(s, true)
      });
		}
	}

  showScore( x, y, message, size )
	{
		let s = 11;
		s += Math.ceil( size * 0.5 );
		if ( s > 35 ) s = 35;

		let font = "vcrosdmn";
		if ( s > 15 ) { font = "finalold"; s += 5; }
		if ( s > 23 ) { font = "sffedora"; s -= 5; }
		if ( s > 31 ) { font = "fadetogr"; }

    //console.log( font );

		let t = new Phaser.GameObjects.BitmapText( this.scene, x, y, font, message, s );
    t.setOrigin(0.5,0);
		t.alpha = 0;
		this.scene.add.existing(t);

    this.scene.tweens.add({
      targets: t,
      alpha: 1,
      duration: 1000,
      ease: 'Quad.easeOut',
    });
    this.scene.tweens.add({
      targets: t,
      alpha: 0,
      duration: 300,
      delay: 1000,
      ease: 'Quad.easeIn',
    });
    this.scene.tweens.add({
      targets: t,
      y: t.y - 25,
      duration: 1300,
      ease: 'Expo.easeOut',
      onComplete: () => this.remove(s, true)
    });
  }

}
