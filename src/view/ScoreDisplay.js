import Phaser from 'phaser'

import InfoDisplay from '../view/InfoDisplay';

export default class ScoreDisplay extends InfoDisplay
{

  static get scoreDisplayWidth() { return 170; }
  static get scoreDisplayHeight() { return 56; }

  constructor(config)
  {
    super(config);

    // the title of the score display
    this.title = null;

    this.text = null;
    this.text = this.scene.add.bitmapText(
      0,
      -18,
      "sffedora",
      "SCORE",
      17);
    this.text.setCenterAlign();
    this.text.setOrigin(0.5);
    this.add( this.text );

    // the score text
    this.scoreText = null;
    this.scoreText = this.scene.add.bitmapText(
      0,
      5,
      "sffedora",
      "0",
      28);
    this.scoreText.setCenterAlign();
    this.scoreText.setOrigin(0.5);
    this.add( this.scoreText );
  }


  defineShield()
  {
    this.shield = this.scene.add.rectangle(
      0,
      0,
      ScoreDisplay.scoreDisplayWidth,
      ScoreDisplay.scoreDisplayHeight,
      0x0000000,
      0.6
    );
    this.add( this.shield );
  }

  get height() { return this.shield.height; }
  get width() { return this.shield.width; }
  get score() { return this.scoreText.text; }


  display( data )
  {
    super.display( data );
    if( 'score' in data )
    {
      this.scoreText.setText( data.score );
    }
  }
}
