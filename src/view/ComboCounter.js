import Phaser from 'phaser'
/*
 * Shows medals for combos.
 */
export default class ComboCounter extends Phaser.GameObjects.Container
{

  static get COMBO_UP() { return "COMBO_UP"; }
  static get COMBO_OVER() { return "COMBO_OVER"; }

  constructor( config )
  {
    super(config.scene);

    // keeps a count of our current combo level
    this.count = 0;

    // an array of the medals to display at each combo level
    this.medals = ( config.medals === null ) ? config.medals: [
      null,
      null,
      null,
      { medal:"medal1", font:"sffedora", size:18 },
      null,
      null,
      { medal:"medal2", font:"finalold", size:32 },
      null,
      null,
      { medal:"medal3", font:"fadetogr", size:42 },
    ];

    // the assets we will display
    this.medal = null;
    this.text = null;

    // where to display them
    this.displayX = 'displayX' in config ? config.displayX : this.scene.cameras.main.centerX;
    this.displayY = 'displayY' in config ? config.displayY : this.scene.cameras.main.centerY;

    // scale the medals as necessary
    this.maxHeight = 240;

    this.scene.add.existing(this);
  }

  reset()
  {
    // if we displayed a medal, emit an event that this combo is over
    let i = 0;
    while( this.medals[i] === null ) i++;
    if( i <= this.count ) this.emit( ComboCounter.COMBO_OVER );

    this.count = 0;
  }

  combo()
  {
    this.count++;
    if( this.medals[ this.count ] !== null )
    {
      this.showMedal();
    }
  }

  showMedal()
  {
    if( this.text !== null ) this.text.destroy();
    if( this.medal !== null ) this.medal.destroy();

    // if a player gets this much, don't worry about displaying anything
    if( this.count >= this.medals.length ) return;

    // position our assets
    let p = this.medals[ this.count ];

    this.medal = this.scene.add.image(this.displayX, this.displayY, p.medal);
    if( this.medal.height > this.maxHeight) this.medal.scale = this.maxHeight / this.medal.height;
		this.medal.alpha = 0;

    this.text = this.scene.add.bitmapText(
      this.displayX,
      this.displayY,
      p.font,
      this.count + "X COMBO",
      p.size);
		this.text.setCenterAlign();
		this.text.setOrigin(0.5);
		this.text.alpha = 0;

    // animate the display and hide
    this.scene.tweens.add({
      targets: this.medal,
      alpha: 0.6,
      duration: 100
    });
    this.scene.tweens.add({
      targets: this.text,
      alpha: 0.8,
      duration: 100
    });
    this.scene.tweens.add({
      targets: [this.medal, this.text],
      alpha: 0,
      duration: 200,
      delay: 3200,
    });

    // emit an event saying we are displaying a medal
    this.emit( ComboCounter.COMBO_UP );
  }

}
