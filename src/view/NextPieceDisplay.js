import Phaser from 'phaser'

import NextPieceGenerator from '../model/NextPieceGenerator';

import GamePiece from '../graphics/GamePiece';
import Bomb from '../graphics/Bomb';
import Brick from '../graphics/Brick';
import Marshmallow from '../graphics/Marshmallow';
import Water from '../graphics/Water';
import Wood from '../graphics/Wood';

export default class NextPieceDisplay extends Phaser.GameObjects.Container
{
  static get shieldBorder() { return 8; }

  constructor(config)
  {
    super(config.scene);

    // the background on which the NextPieceDisplay is displayed
    this.shield = this.scene.add.rectangle(
      0,
      0,
      2 * GamePiece.gamePieceWidth + 2 * NextPieceDisplay.shieldBorder,
      1 * GamePiece.gamePieceWidth + 2 * NextPieceDisplay.shieldBorder,
      0x0000000,
      0.6
    );
    this.add( this.shield );

    this.scene.add.existing(this);

    // the pieces we are currently showing
    this.gamePieces = null;
  }

  get height() { return this.shield.height; }
  get width() { return this.shield.width; }

  hidePieces()
  {
    let delay = 0;

    if( this.gamePieces !== null )
    {
      this.gamePieces[0].destroy();
      this.gamePieces[1].destroy();
      delay = 150;
    } else {
      this.gamePieces = [null, null];
    }

    return delay;
  }


  showPieces( type1, type2 )
  {
    let delay = this.hidePieces();

    this.gamePieces[0] = this.getPieceSprite(type1);
    this.gamePieces[0].x = this.shield.x - GamePiece.gamePieceWidth/2;
    this.gamePieces[0].y = this.shield.y;
    this.gamePieces[0].alpha = 0;

    this.gamePieces[1] = this.getPieceSprite(type2);
    this.gamePieces[1].x = this.shield.x + GamePiece.gamePieceWidth/2;
    this.gamePieces[1].y = this.shield.y;
    this.gamePieces[1].alpha = 0;

    //console.log( this.gamePieces[0].x, this.gamePieces[0].y );
    //console.log( this.gamePieces[1].x, this.gamePieces[1].y );

    this.add( this.gamePieces[0] );
    this.add( this.gamePieces[1] );

    this.scene.tweens.add({
      targets: [ this.gamePieces[0], this.gamePieces[1] ],
      alpha: 1,
      duration: 150,
      delay: delay
    });
  }

  getPieceSprite( type )
  {
    switch( type )
		{
			case NextPieceGenerator.BRICK:
				return new Brick({scene:this.scene});
				break;
			case NextPieceGenerator.WOOD:
				return new Wood({scene:this.scene});
				break;
			case NextPieceGenerator.FIRE:
				return new Wood({scene:this.scene, burning:true});
				break;
			case NextPieceGenerator.WATER:
				return new Water({scene:this.scene});
				break;
			case NextPieceGenerator.ICE:
				return new Water({scene:this.scene, frozen:true});
				break;
			case NextPieceGenerator.MARSHMALLOW:
				return new Marshmallow({scene:this.scene});
				break;
			case NextPieceGenerator.BOMB:
				return new Bomb({scene:this.scene});
				break;
		}
  }


  loadNewPieces( gp1, gp2 )
  {
    const o1 = this.gamePieces === null ? null : this.gamePieces[0].type;
    const o2 = this.gamePieces === null ? null : this.gamePieces[1].type;

    this.showPieces( gp1, gp2 );

    return [o1, o2];
  }


  getPieceTypes(num)
  {
    return this.gamePieces[num].type;
  }


}
