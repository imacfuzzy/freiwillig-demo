import Phaser from 'phaser'

import CountdownBadge from '../graphics/CountdownBadge';
import GamePiece from '../graphics/GamePiece';

import GameGrid from '../view/GameGrid';

export default class ThrowCountdownDisplay extends Phaser.GameObjects.Container
{
  static get shieldBorder() { return 3; }

  constructor(config)
  {
    super(config.scene);

    // the maximum number of badges to show
    this.maxBadges = ('maxBadges' in config) ? config.maxBadges : 6;

    // the background on which the GameGrid is displayed
    this.shield = this.scene.add.rectangle(
      0,
      0,
      this.maxBadges * GamePiece.gamePieceWidth + 2 * GameGrid.shieldBorder,
      GamePiece.gamePieceWidth + 2 * GameGrid.shieldBorder,
      0x0000000,
      0.6
    );
    this.add( this.shield );

    /*
    let shape = this.scene.make.graphics();
    //  Create a hash shape Graphics object
    shape.fillStyle(0xffffff);
    //  You have to begin a path for a Geometry mask to work
    shape.beginPath();
    shape.fillRect(
      0 - this.width *0.5,
      0 - this.height * 0.5,
      this.shield.width,
      this.shield.height
    );
    shape.setVisible(false);
    this.add(shape);


    //this.shield.setMask( shape.createGeometryMask() );

    this.badgeMask = new Phaser.Display.Masks.BitmapMask(this.scene, shape);
    */

    // if we are animating a badge, this will be true
    this.updating = false;

    // the number of badges we are currently working towards
    this.queued = 0;

    this.scene.add.existing(this);
  }

  get height() { return this.shield.height; }
  get width() { return this.shield.width; }

  get children()
  {
    return this.list;
  }

  get currentCount()
  {
    return this.children.length - 1;
  }

  get currentBadgeNum()
  {
    return this.children[ this.currentCount ].number;
  }

  get badgeCount()
  {
    return this.children.length - 1;
  }

  moveOn( badge, space )
  {
    //this.scene.add(badge);
    this.add(badge);
    badge.x = -1 * this.scene.sys.game.canvas.width + space * GamePiece.gamePieceWidth;
    badge.roll(1,1);

    this.scene.tweens.add({
      targets: badge,
      x: this.shield.x - this.shield.width * 0.5 + ThrowCountdownDisplay.shieldBorder + GamePiece.gamePieceWidth * (space + 0.5),
      duration: 1000,
      ease: 'Sine.easeInOut',
    });

    return 1000;
  }

  moveOff( badge, space )
  {
    //badge.roll(1,1);
    this.scene.tweens.add({
      targets: badge,
      x: this.scene.sys.game.canvas.width + 350 + space * GamePiece.gamePieceWidth,
      duration: 1000,
      //scale: GamePiece.gamePieceWidth / badge.width,
      ease: 'Sine.easeInOut',
      onComplete: () => badge.destroy()
    });
  }

  removeABadge()
  {
    // if there are no badges to remove, return
    if( !this.badgeCount ) return;

    if( this.currentBadgeNum > this.maxBadges ) // remove the single badge that is showing
    {
      if( this.badgeCount ) this.moveOff(
        this.children[ this.currentCount ],
        this.maxBadges / 2
      );
    }
    else // remove a single badge from the line
    {
      if( this.badgeCount ) this.moveOff( this.children[ this.currentCount ], 1 );
    }
  }

  addABadge(num)
  {

    if( num > this.maxBadges )
    {
      let mid = this.maxBadges % 2 ? this.maxBadges / 2 : this.maxBadges / 2 - 0.5;
      this.scene.time.delayedCall(
				this.moveOn( new CountdownBadge({scene:this.scene, number:num, mask:this.badgeMask}), mid ),
				() => this.children[ this.currentCount ].enlarge()
			)
    }
    else if( num === this.maxBadges || !this.currentCount )
    {
      for ( let i = 0; i < num; i++ ) {
        this.moveOn( new CountdownBadge({scene:this.scene, number:i+1, mask:this.badgeMask}), i );
      }
    }
    else // we actually need to take away a badge!
    {
      this.removeABadge();
    }
  }

  show( num )
  {
    // make sure we are not currently animating
    if( this.updating )
    {
      this.queued++;
      return;
    }
    this.updating = true;

    this.removeABadge();

    this.addABadge(num);

    this.updating = false;

    // if there are moves queued, do them, as well
    if( this.queued )
    {
      this.queued--;
      this.show(num-1);
    }
  }

}
