import Phaser from 'phaser'

import GamePiece from '../graphics/GamePiece';
import Wood from '../graphics/Wood';
import Water from '../graphics/Water';

import FallingPiece from '../model/FallingPiece';
import Explode from '../model/reactions/Explode';

export default class GameGrid extends Phaser.GameObjects.Container
{

  static get shieldBorder() { return 3; }

  static get TIME_BUFFER() { return 200; }
  static get TIME_MOVE() { return 150; }
  static get GRAVITY() { return 300; }
  static get DISAPPEAR_DELAY() { return 100; }
  static get FREEZE_DELAY() { return 200; }
  static get FREEZE_REACTION() { return 140; }
  static get EXTINGUISH_DELAY() { return 90; }
  static get BURN_DELAY() { return 200; }
  static get BURN_REACTION() { return 160; }
  static get TOAST_DELAY() { return 250; }
  static get TOAST_SHOCK() { return 750; }
  static get TOAST_REACTION() { return 40; }

  static get BOMB_LEVEL_JUMP() { return 120; }
  static get BOMB_DELAY() { return 225; }
  static get BOMB_SHOCK() { return 750; }
  static get BOMB_REACTION() { return 300; }

  static get DROP() { return "DROP"; }
  static get RIGHT() { return "RIGHT"; }
  static get LEFT() { return "LEFT"; }
  static get ROTATE() { return "ROTATE"; }

  static get GAME_NEWROUND() { return "GAME_NEWROUND"; }
  static get GAME_SET() { return "GAME_SET"; }
  static get GAME_BOUNCE() { return "GAME_BOUNCE"; }

  static get PIECE_FALLING() { return "PIECE_FALLING"; }
  static get PIECE_FREEZE() { return "PIECE_FREEZE"; }
  static get PIECE_CLEAR() { return "PIECE_CLEAR"; }
  static get PIECE_EXTINGUISH() { return "PIECE_EXTINGUISH"; }
  static get PIECE_THAW() { return "PIECE_THAW"; }
  static get PIECE_BURN() { return "PIECE_BURN"; }
  static get PIECE_TOAST() { return "PIECE_TOAST"; }
  static get PIECE_ACTIVATE() { return "PIECE_ACTIVATE"; }
  static get PIECE_EXPLODE() { return "PIECE_EXPLODE"; }

  constructor(config)
  {
    super(config.scene);

    // the dimensions, by GamePiece
    this.gridWidth = ( 'gridWidth' in config ) ? config.gridWidth : 6;
    this.gridHeight = ( 'gridHeight' in config ) ? config.gridHeight : 11;

    // the background on which the GameGrid is displayed
    this.shield = this.scene.add.rectangle(
      0,
      0,
      this.gridWidth * GamePiece.gamePieceWidth + 2 * GameGrid.shieldBorder,
      this.gridHeight * GamePiece.gamePieceWidth + 2 * GameGrid.shieldBorder,
      0x0000000,
      0.6
    );
    this.add( this.shield );

    this.x = this.scene.cameras.main.centerX;
    this.y = this.scene.cameras.main.centerY;

    // the nested array that contains all GamePieces on the grid
    this.gamePieces = [];
    for( let i = 0; i<this.gridWidth; i++ ) {
      this.gamePieces.push([]);
      for( let j = 0; j<this.gridHeight; j++ ) {
        this.gamePieces[i].push(0);
      }
    }

    // the falling pair of GamePieces
    this.piece1 = null;
    this.piece2 = null;

    // the resting GamePieces
    this.restingPieces = new Phaser.GameObjects.Group(this.scene);

    // a clock for tracking when the fallingPiece should move down
    this.autoDropTime = ( config.autoDropTime === null ) ? config.autoDropTime : 600;

    this.autoDropTimer = null;

    this.scene.add.existing(this);

    this.nextPieceArray = [null, null];

  }


  get height() { return this.shield.height; }
  get width() { return this.shield.width; }

  check()
  {
    this.emit( GameGrid.GAME_NEWROUND, { grid: this.gamePieces } );
  }


  restartAutoDropTimer()
  {
    if( this.autoDropTimer !== null ) this.autoDropTimer.remove();
    this.autoDropTimer = this.scene.time.addEvent({
        delay: this.autoDropTime,
        callback: () => this.dropFallingPiece(),
        loop: true
    });
  }

  speedUpAutoDropTimer()
  {
    if( this.autoDropTime < 250 ) return;
    this.autoDropTime -= 60;
    this.restartAutoDropTimer();
  }

  isSpotFree( i, j )
  {
    if( i < 0 || i >= this.gamePieces.length || j < 0 || j >= this.gamePieces[i].length ) return false;
    return (this.gamePieces[i][j] === 0);
  }


  // TODO:
  // make this react to different size grids
  isDropAreaFree() {
		return (this.isSpotFree(2,0) && this.isSpotFree(3,0));
	}


  getPieceX( x ) { return this.shield.x - this.shield.width/2 + GamePiece.gamePieceWidth * (x+0.5) + GameGrid.shieldBorder; }
  getPieceY( y ) { return this.shield.y - this.shield.height/2 + GamePiece.gamePieceWidth * (y+0.5) + GameGrid.shieldBorder; }


  /*
   * Put the pieces from a throw into the game.
   */
  addThrow( throwArr )
  {
    let delay = 0;

    for( let i = 0; i < throwArr.length; i++ )
    {
      for( let j=0; j < throwArr[i].length; j++ )
      {
        // we can't add a throw piece where a game piece is already present
        if( this.gamePieces[i][j] !== 0 ) {
          throwArr[i][j].destroy();
          continue;
        }

        this.gamePieces[i][j] = throwArr[i][j];
        this.gamePieces[i][j].x = this.getPieceX( i );
    		this.gamePieces[i][j].y = this.getPieceY( j );
        this.gamePieces[i][j].alpha = 0;
        this.add( this.gamePieces[i][j] );

        this.scene.tweens.add({
          targets: this.gamePieces[i][j],
          alpha: 1,
          duration: 300,
          delay: i * 50 + j * 30
        });
        delay = Math.max( delay, i*50 );
      }
    }
    return delay;
  }


  addPiece()
  {
    // TODO:
    // make this react to different size grids
		this.fallingPiece = new FallingPiece( 2, 0, 3, 0 );

		this.piece1 = this.nextPieceArray[0];
		this.piece1.x = this.getPieceX( this.fallingPiece.piece1.x );
		this.piece1.y = this.getPieceY( this.fallingPiece.piece1.y );
    this.add( this.piece1 );

		this.piece2 = this.nextPieceArray[1];
		this.piece2.x = this.getPieceX( this.fallingPiece.piece2.x );
		this.piece2.y = this.getPieceY( this.fallingPiece.piece2.y );
    this.add( this.piece2 );

		this.restartAutoDropTimer();

		return true;
	}


  dropFallingPiece( delay = false )
  {
		if( delay && !this.moveFallingPiece(GameGrid.DROP) ) {
      scene.time.delayedCall(
        1000,
        () => this.releaseFallingPiece()
      );
		}
    else if ( !this.moveFallingPiece(GameGrid.DROP) ) {
      this.releaseFallingPiece();
		}
	}


  moveFallingPieceLeft()
  {
		this.moveFallingPiece(GameGrid.LEFT);
	}


	moveFallingPieceRight()
  {
		this.moveFallingPiece(GameGrid.RIGHT);
	}


  rotateFallingPiece()
  {
		if ( this.fallingPiece !== null && !this.moveFallingPiece(GameGrid.ROTATE) )
		{
			// check to see about moving the falling piece to the side to make rotating possible
			if ( this.fallingPiece.piece1.y === 0 && this.fallingPiece.piece2.y === 0 )
			{
				if( this.moveFallingPiece( GameGrid.DROP ) ) this.moveFallingPiece( GameGrid.ROTATE );
			}
			else if ( this.fallingPiece.piece1.y < this.fallingPiece.piece2.y && this.fallingPiece.piece1.x > 0 )
			{
				if ( this.isSpotFree( this.fallingPiece.piece1.x - 1, this.fallingPiece.piece1.y ) ) {
					this.moveFallingPiece( GameGrid.LEFT );
					this.moveFallingPiece( GameGrid.ROTATE );
				}
			}
			else if ( this.fallingPiece.piece1.y > this.fallingPiece.piece2.y && this.fallingPiece.piece1.x < this.gridWidth-1 )
			{
				if ( this.isSpotFree( this.fallingPiece.piece1.x + 1, this.fallingPiece.piece1.y ) ) {
					this.moveFallingPiece( GameGrid.RIGHT );
					this.moveFallingPiece( GameGrid.ROTATE );
				}
			}
		}
	}


  moveFallingPiece( movement )
  {
		if ( this.piece1 === null
      || this.piece2 === null ) return false;

		var fp = this.fallingPiece.clone();
		switch( movement ) {
			case GameGrid.RIGHT: fp.moveRight(); break;
			case GameGrid.LEFT: fp.moveLeft(); break;
			case GameGrid.ROTATE: fp.rotate(); break;
			case GameGrid.DROP: fp.drop(); break;
		}

		if ( this.isSpotFree( fp.piece1.x, fp.piece1.y )
			&& this.isSpotFree( fp.piece2.x, fp.piece2.y ) ) {

				// move the current falling piece down
				switch( movement ) {
					case GameGrid.RIGHT:
            this.fallingPiece.moveRight();
            break;
					case GameGrid.LEFT:
            this.fallingPiece.moveLeft();
            break;
					case GameGrid.ROTATE:
            this.fallingPiece.rotate();
            break;
					case GameGrid.DROP:
            this.fallingPiece.drop();
            this.restartAutoDropTimer();
            break;
				}
				this.animateFallingPiece();
				return true;
			}

		return false;
	}

  animateFallingPiece()
  {
    let ease = 'Sine.easeInOut';

    this.scene.tweens.add({
			targets: this.piece1,
      x: this.getPieceX(this.fallingPiece.piece1.x),
      y: this.getPieceY(this.fallingPiece.piece1.y),
			duration: GameGrid.TIME_MOVE,
      ease: ease
		});
    this.scene.tweens.add({
			targets: this.piece2,
      x: this.getPieceX(this.fallingPiece.piece2.x),
      y: this.getPieceY(this.fallingPiece.piece2.y),
			duration: GameGrid.TIME_MOVE,
      ease: ease
		});
	}


  releaseFallingPiece()
  {
    if( this.fallingPiece === null ) return;

    // stop the piece from dropping
    this.autoDropTimer.paused = true;

    // set piece1 and piece2 in gamePieces[][]
    this.gamePieces[ this.fallingPiece.piece1.x ][ this.fallingPiece.piece1.y ] = this.piece1;
    this.gamePieces[ this.fallingPiece.piece2.x ][ this.fallingPiece.piece2.y ] = this.piece2;

    // notify that the piece has been set down
    this.emit( GameGrid.GAME_SET );

    this.fallingPiece = null;
    this.piece1 = null;
    this.piece2 = null;

    // signal that the piece has set
    this.check();
  }


  pieceFalling( i, j, spaces ) {

    let time = GameGrid.GRAVITY * spaces;

    // listen for the bouncing of the affected game pieces
    this.gamePieces[i][j].bouncing(time);
    /*
    this.gamePieces[i][j].on( GamePiece.BOUNCE, () => this.emit( GameGrid.GAME_BOUNCE ) );

    this.scene.time.delayedCall(
      time + 1,
      () => {
        this.gamePieces[i][j+spaces].bouncing(false);
        this.gamePieces[i][j+spaces].off( GamePiece.BOUNCE, () => this.emit( GameGrid.GAME_BOUNCE ) );
      }
    );
    */

    this.scene.tweens.add({
      targets: this.gamePieces[i][j],
      duration: time,
      y: this.getPieceY(j+spaces),
      ease: 'Bounce.easeOut'
    });

		// move the piece in the grid
		this.gamePieces[i][j+spaces] = this.gamePieces[i][j];
		this.gamePieces[i][j] = 0;

		return time;
	}


  pieceClear( i, j, delayFactor ) {
    let time = GameGrid.DISAPPEAR_DELAY * delayFactor;
    const p = this.gamePieces[i][j];

    // clear the space in the gameGrid
    this.gamePieces[i][j] = 0;

    this.scene.time.delayedCall(
      time,
      () => {
        this.emit( GameGrid.PIECE_CLEAR, { x:this.x + this.getPieceX(i), y:this.y + this.getPieceY(j) } );
        p.destroy();
      }
    );
    return time;
  }

  pieceFreeze( i, j, delayFactor ) {
    let time = GameGrid.FREEZE_DELAY * delayFactor;
    const p = this.gamePieces[i][j];

    this.scene.time.delayedCall(
      time,
      () => {
        this.emit( GameGrid.PIECE_FREEZE, { x:this.x + this.getPieceX(i), y:this.y + this.getPieceY(j) } );
        p.freeze( GameGrid.FREEZE_REACTION );
      }
    );
    return time + GameGrid.FREEZE_REACTION;
  }


  pieceExtinguish( i, j ) {
    const p = this.gamePieces[i][j];
    switch( p.type )
    {
      case "fire":
        this.emit( GameGrid.PIECE_EXTINGUISH, { x:this.x + this.getPieceX(i), y:this.y + this.getPieceY(j) } );
        // clear the space in the gameGrid
        this.gamePieces[i][j] = 0;
        p.destroy();
        break;
      case "water":
        this.emit( GameGrid.PIECE_EXTINGUISH, { x:this.x + this.getPieceX(i), y:this.y + this.getPieceY(j) } );
        // clear the space in the gameGrid
        this.gamePieces[i][j] = 0;
        p.destroy();
        break;
      case "ice":
        this.emit( GameGrid.PIECE_THAW, { x:this.x + this.getPieceX(i), y:this.y + this.getPieceY(j) } );
        p.unfreeze();
        break;
    }
    return GameGrid.EXTINGUISH_DELAY;
  }


  pieceBurn( i, j, delayFactor ) {
    let time = GameGrid.BURN_DELAY * delayFactor;
    const p = this.gamePieces[i][j];

    this.scene.time.delayedCall(
      time,
      () => {
        this.emit( GameGrid.PIECE_BURN, { x:this.x + this.getPieceX(i), y:this.y + this.getPieceY(j) } );
        p.burn( GameGrid.BURN_REACTION );
      }
    );
    return time + GameGrid.BURN_REACTION;
  }


  pieceToast( i, j ) {
    const p = this.gamePieces[i][j];

    // toast the mallow
    this.scene.time.delayedCall(
      GameGrid.TOAST_DELAY,
      () => {
        this.emit( GameGrid.PIECE_TOAST, { x:this.x + this.getPieceX(i), y:this.y + this.getPieceY(j) } );
        p.toast( GameGrid.TOAST_REACTION );
      }
    );

    // clear the space in the gameGrid
    this.gamePieces[i][j] = 0;

    // clear the mallow
    this.scene.time.delayedCall(
      GameGrid.TOAST_DELAY + GameGrid.TOAST_SHOCK,
      () => {
        this.emit( GameGrid.PIECE_CLEAR, { x:this.x + this.getPieceX(i), y:this.y + this.getPieceY(j) } );
        p.destroy();
      }
    );

    return GameGrid.TOAST_DELAY + GameGrid.TOAST_SHOCK + GameGrid.TOAST_REACTION;
  }


  pieceExplode( i, j, level, type )
  {
    switch(type)
    {
      case Explode.EXPLODE_BOMB:
        return this.explodeBomb( i, j, level );
      case Explode.EXPLODE_OTHER:
        return this.explodeOther( i, j, level );
    }
    return -1;
  }

  // helper function for pieceExplode()
  explodeBomb( i, j, level )
  {
    let additionalTime = level * GameGrid.BOMB_LEVEL_JUMP;
    const p = this.gamePieces[i][j];

    // activate the bomb
    this.scene.time.delayedCall(
      additionalTime + GameGrid.BOMB_DELAY,
      () => {
        this.emit( GameGrid.PIECE_ACTIVATE, { x:this.x + this.getPieceX(i), y:this.y + this.getPieceY(j) } );
        p.ignite( GameGrid.BOMB_REACTION );
      }
    );

    // clear the space in the gameGrid
    this.gamePieces[i][j] = 0;

    // explode the bomb
    this.scene.time.delayedCall(
      additionalTime + GameGrid.BOMB_DELAY + GameGrid.BOMB_SHOCK,
      () => {
        this.emit( GameGrid.PIECE_EXPLODE, { x:this.x + this.getPieceX(i), y:this.y + this.getPieceY(j) } );
        p.destroy();
      }
    );

    return additionalTime + GameGrid.BOMB_DELAY + GameGrid.BOMB_SHOCK + GameGrid.BOMB_REACTION;
  }

  // helper function for pieceExplode()
  explodeOther( i, j, level )
  {
    let additionalTime = level * GameGrid.BOMB_LEVEL_JUMP;
    const p = this.gamePieces[i][j];

    // activate the bomb
    this.scene.time.delayedCall(
      additionalTime + GameGrid.BOMB_DELAY + GameGrid.BOMB_SHOCK,
      () => {
        this.emit( GameGrid.PIECE_CLEAR, { x:this.x + this.getPieceX(i), y:this.y + this.getPieceY(j) } );
        p.destroy( GameGrid.BOMB_SHOCK );
      }
    );

    // clear the space in the gameGrid
    this.gamePieces[i][j] = 0;

    return additionalTime + GameGrid.BOMB_DELAY + GameGrid.BOMB_SHOCK;
  }


  update()
  {
    for( let i=0; i<this.list.length; i++ )
    {
      this.list[i].update();
    }
  }

}
