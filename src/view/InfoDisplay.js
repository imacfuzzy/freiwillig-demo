import Phaser from 'phaser'

export default class InfoDisplay extends Phaser.GameObjects.Container
{
  static get shieldBorder() { return 8; }

  constructor(config)
  {
    super(config.scene);

    // the background on which the InfoDisplay is displayed
    this.shield = null;
    this.defineShield();

    this.scene.add.existing(this);
  }

  defineShield() {}

  display( data ) {}
}
