import Phaser from 'phaser'

export default class GameOverDisplay extends Phaser.GameObjects.Container
{
  constructor(config)
  {
    super(config.scene);

    // the text object with the numbers for counting down
    this.gameOverText = null;

    // shades the screen
    this.shade = null;

    // the enter button logo
    this.enterButton = null;

    // where to display our countdownText
    this.displayX = 'displayX' in config ? config.displayX : this.scene.cameras.main.centerX;
    this.displayY = 'displayY' in config ? config.displayY : this.scene.cameras.main.centerY;

    this.scene.add.existing(this);
  }

  show()
  {
    this.shade = this.scene.add.rectangle(
      this.scene.cameras.main.centerX,
      this.scene.cameras.main.centerY,
      this.scene.sys.game.canvas.width,
      this.scene.sys.game.canvas.height,
      0x0000000,
      0.6
    );
    this.shade.alpha = 0;
    this.add( this.shade );

    this.gameOverText = this.scene.add.bitmapText(
      this.scene.cameras.main.centerX,
      this.displayY,
      'vcrosdmn',
      "GAME OVER",
      80);
		this.gameOverText.setCenterAlign();
		this.gameOverText.setOrigin(0.5);
    this.gameOverText.alpha = 0;

    this.add(this.gameOverText);


    this.enterButton = this.scene.add.image(
      this.displayX,
      this.scene.cameras.main.centerY + this.displayY,
      "enter"
    );
    this.add( this.enterButton );
    this.pulseButtonIn();


    this.scene.tweens.add({
      targets: this.gameOverText,
      alpha: 1,
      duration: 1500
    });
    this.scene.tweens.add({
      targets: this.shade,
      alpha: 0.6,
      duration: 1500
    });
  }

  pulseButtonIn()
  {
    this.scene.tweens.add({
      targets: this.enterButton,
      scale: 0.5,
      ease: 'Bounce.easeOut',
      duration: 500,
      delay: 1000,
      onComplete: () => this.pulseButtonOut()
    });
  }

  pulseButtonOut()
  {
    this.scene.tweens.add({
      targets: this.enterButton,
      scale: 1,
      ease: 'Bounce.easeOut',
      duration: 500,
      delay: 1000,
      onComplete: () => this.pulseButtonIn()
    });
  }
}
