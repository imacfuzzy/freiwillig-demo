class Options {

  constructor()
  {
    // how loud the game music should play
    this._musicVolume = 0.5;

    // how loud soundfx should play in all scenes
    this._soundfxVolume = 0.7;

  }

  get musicVolume()
  {
    return this._musicVolume;
  }

  set musicVolume( val )
  {
    this._musicVolume = val;
  }

  get soundfxVolume()
  {
    return this._soundfxVolume;
  }

  set soundfxVolume( val )
  {
    this._soundfxVolume = val;
  }
}

export default new Options();
