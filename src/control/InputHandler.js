import Phaser from 'phaser'

/*
 * Processes input.
 */
export default class InputHandler extends Phaser.Events.EventEmitter
{
  static get DOWN() { return "DOWN"; }
  static get UP() { return "UP"; }
  static get LEFT() { return "LEFT"; }
  static get RIGHT() { return "RIGHT"; }
  static get ENTER() { return "ENTER"; }

  constructor()
  {
    super();
  }

  handle( value )
  {
    switch( value )
    {
      case "ArrowDown":
        this.emit( InputHandler.DOWN );
        break;
      case "ArrowUp":
        this.emit( InputHandler.UP );
        break;
      case "ArrowLeft":
        this.emit( InputHandler.LEFT );
        break;
      case "ArrowRight":
        this.emit( InputHandler.RIGHT );
        break;
      case "Enter":
        this.emit( InputHandler.ENTER );
        break;
    }
  }
}
