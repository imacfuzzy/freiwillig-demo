import Phaser from 'phaser'

export default class GamePiece extends Phaser.GameObjects.Container
{
  static get gamePieceWidth()
  {
    return 48;
  }

  static get BOUNCE() { return "BOUNCE"; }

  constructor(config)
  {
    super(config.scene);

    // marks what kind of GamePiece this is
    this.type = config.type;

    // set standard dimensions for GamePiece
    this.gamePieceWidth = GamePiece.gamePieceWidth;

    // set standard transition elements
    this.transitionTime = 200;
    this.transitionEase = 'Quad.easeInOut';

    // does this piece clear if connected with enough other pieces?
    this.clears = false;
    if( config.clears !== null ) this.clears = config.clears;

    this.images = config.images;

    this.scene.add.existing(this);

    for(let i=0; i<this.images.length; i++) {
      let s = this.scene.add.image(0, 0, this.images[i]);
      s.scale = this.gamePieceWidth / s.width;
      if( i > 0 ) s.alpha = 0;
      this.add(s);
    }

    this.trackingBounce = false;
    this.fallingDown = true;
    this.previousY = null;
  }

  destroy()
  {
    this.scene.tweens.add({
      targets: this,
      alpha: 0,
      duration: 150,
      onComplete: () => { super.destroy(); }
    })
  }

  update( args )
  {
    super.update(args);

    if( this.trackingBounce )
    {
      if( this.previousY !== null )
      {
        if( this.y > this.previousY ) this.fallingDown = true;
        if( this.fallingDown && this.y < this.previousY ) {
          this.fallingDown = false;

          // TODO:
          // this is a workaround. find a better solution
          //this.emit( GamePiece.BOUNCE );
          this.scene.soundEffects.bounce.play();
        }
      }
      this.previousY = this.y;
    }
  }

  bouncing( time )
  {
    this.trackingBounce = true;

    this.scene.time.delayedCall(
      time,
      () => this.trackingBounce = false
    );
  }

}
