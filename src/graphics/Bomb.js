import Phaser from 'phaser'

import GamePiece from '../graphics/GamePiece';

export default class Bomb extends GamePiece
{
  constructor(config)
  {
    config.images = ["bomb","redbomb"];
    config.type = "bomb";
    super(config);

    this.ignited = false;
  }

  ignite( delay = 0 )
  {
    this.ignited = true;
    this.list[1].alpha = 1;

    this.scene.tweens.add({
      targets: this.first,
      alpha: 0,
      duration: this.transitionTime,
      ease: this.transitionEase,
      delay: delay
    });
  }
}
