import Phaser from 'phaser'

export default class Cinder extends Phaser.GameObjects.Sprite
{
  constructor(config)
  {
    super(config.scene, config.x, config.y, "cinder");
    config.scene.add.existing(this);

    this.rnd = Phaser.Math.RND;
    this.scale = ( this.rnd.frac() * 12 ) / this.width;
    this.alpha = 0;

    this.tweens = config.scene.tweens;
  }

  generateX()
  {
    return this.rnd.normal() * 95;
  }

  pulseIn()
  {
    this.tweens.add({
			targets: this,
			alpha:1,
      x: this.x + this.generateX(),
      width:2,
      height:2,
			duration:3000 + this.rnd.frac() * 3000,
			ease: 'Quad.easeInOut',
			onComplete: () => this.pulseOut(),
		});
  }

  pulseOut()
  {
    this.tweens.add({
			targets: this,
			alpha:0,
      x: this.x + this.generateX(),
      width:10,
      height:10,
			duration:3000 + this.rnd.frac() * 3000,
			ease: 'Quad.easeInOut',
			onComplete: () => this.pulseIn(),
		});
  }
}
