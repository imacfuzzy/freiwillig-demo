import Phaser from 'phaser'

export default class TypeTextBox extends Phaser.GameObjects.Container
{
  static get shieldBorder() { return 18; }

  static get DONE() { return "DONE"; }
  static get WAITING_FOR_ENTER() { return "WAITING_FOR_ENTER"; }
  static get SHOWING_TEXT() { return "SHOWING_TEXT"; }

  constructor( config )
  {
    super(config.scene);

    /*
     * Content should be an array of strings to display.
     * [ "Example text 1.", "This is a second example." ]
     */
    if( !('content' in config) ) console.error("config.content required for TypeTextBox");
    this.content = config.content;
    this.count = 0;

    // data for adding characters, typewriter-style
    this.letter = 0;
    this.letterTimer = null;

    // the assets we will display
    this.shield = null;
    this.text = null;

    // where to display them
    this.displayX = 'displayX' in config ? config.displayX : this.scene.cameras.main.centerX;
    this.displayY = 'displayY' in config ? config.displayY : this.scene.cameras.main.centerY;

    this.shieldWidth = 'shieldWidth' in config ? config.shieldWidth : this.scene.sys.game.canvas.width - 2 * TypeTextBox.shieldBorder;
    this.shieldHeight = 'shieldHeight' in config ? config.shieldHeight : this.scene.sys.game.canvas.height - 2 * TypeTextBox.shieldBorder;
    //console.log('shieldWidth' in config, this.shieldWidth);

    this.displayFont = 'displayFont' in config ? config.displayFont : 'vcrosdmn';
    this.displayFontSize = 'displayFontSize' in config ? config.displayFontSize : 14;
    this.displaySpeed = 'displaySpeed' in config ? config.displaySpeed : 50;


    // get some info for word wrap functionality
    // https://www.html5gamedevs.com/topic/39609-i-need-to-crop-text-or-wordwrap-using-bitmaptext/
    this.wordWrapWidth = this.shieldWidth - 2 * TypeTextBox.shieldBorder;
    let testText = this.scene.add.bitmapText(-200, -200, this.displayFont, "AB", this.displayFontSize);
    this.letterWidth = testText.getTextBounds().global.width / 2; // divide by two because two letters were added to the test text
    this.maxLetters = this.wordWrapWidth / this.letterWidth;
    testText.destroy();

    for( let i = 0; i<this.content.length; i++ )
    {
        this.content[i] = this.addLineBreaks( this.content[i], this.maxLetters );
        //console.log( "content: " + this.content[i] );
    }

    this.scene.add.existing(this);
  }

  addLineBreaks(text, maxLetters) {
    const split = text.split(/( )/g);
    let lines = [];

    function nextLine() {
      let newLine = "";
      while (`${newLine} ${split[0]}`.length < maxLetters && split.length) {
        newLine += split.shift();
      }
      lines.push(newLine.trim());
      if (split.length) {
        nextLine();
      }
    }

    nextLine();

    return lines.join("\n\n");
  }

  show()
  {
    this.showShield( () => this.showText() );
  }

  showShield( callback = null )
  {
    this.shield = this.scene.add.rectangle(this.displayX, this.displayY, this.shieldWidth, this.shieldHeight, 0x0000000, 0.6);
    this.shield.scale = 0.01;
    this.add( this.shield );

    this.scene.tweens.add({
      targets: this.shield,
      duration: 600,
      scale: 1,
      ease: 'Bounce.easeOut',
      onComplete: callback
    });
  }

  showText()
  {
    if( this.count >= this.content.length ) return this.close();

    this.emit( TypeTextBox.SHOWING_TEXT );

    this.text = this.scene.add.bitmapText(
      this.displayX,
      this.displayY,
      this.displayFont,
      this.content[this.count],
      this.displayFontSize);

    this.letter = 0;
    this.text.setLeftAlign();
    this.text.setOrigin(0);
    this.text.fontData.lineHeight = 55;
    this.add( this.text );

    this.text.x = this.shield.x - this.shield.width / 2 + TypeTextBox.shieldBorder;
    this.text.y = this.shield.y - this.shield.height / 2 + TypeTextBox.shieldBorder;
    this.text.setText("");

    this.letterTimer = this.scene.time.addEvent({
      delay: this.displaySpeed,
      callback: () => this.addALetter(),
      repeat: this.content[this.count].length + 1,
    });
  }


  close()
  {
    this.scene.tweens.add({
      targets: this.text,
      alpha: 0,
      duration: 300,
      onComplete: () => {
        this.text.destroy();
        this.scene.tweens.add({
          targets: this.shield,
          scale: 0.01,
          duration: 300,
          ease: 'Quad.easeOut',
          onComplete: () => {
            this.shield.destroy()
            this.emit(TypeTextBox.DONE);
          }
        });
      }
    });
  }

  addALetter()
  {
    if( this.letter > this.content[this.count].length ) return this.waitForEnter();

    this.text.setText( this.content[this.count].slice(0, ++this.letter) );
  }


  skip()
  {
    this.letterTimer.remove();
    this.text.setText( this.content[this.count] );
    this.waitForEnter();
  }


  waitForEnter()
  {
    this.emit(TypeTextBox.WAITING_FOR_ENTER);
  }

  next()
  {
    this.count++;
    this.scene.tweens.add({
      targets:this.text,
      alpha: 0,
      duration: 100,
      onComplete: () => this.showText()
    });
  }

}
