import Phaser from 'phaser'

import GamePiece from '../graphics/GamePiece';

export default class CountdownBadge extends Phaser.GameObjects.Container
{
  static get ENLARGED_WIDTH() { return 80; }

  constructor(config)
  {
    super(config.scene);

    // the badge
    this.badgeImage = this.scene.add.image(0, 0, "badge");
    this.badgeImage.scale = GamePiece.gamePieceWidth / this.badgeImage.width;
    this.add( this.badgeImage );

    let numberToDisplay = 'number' in config ? config.number : '?';

    // the number we are displaying on top of the badge
    this.numberText = this.scene.add.bitmapText(
      -3,
      -5,
      "black",
      numberToDisplay,
      15);

    this.numberText.setOrigin(0.5);
    this.add( this.numberText );

    /*
    // if a mask has been passed in, assign it to contained items
    if( 'mask' in config && config.mask !== null )
    {
      console.log(config.mask);
      this.setMask(config.mask);
      //this.badgeImage.setMask(config.mask);
      //this.numberText.setMask(config.mask);
    }
    */
  }

  roll( rotations, time )
  {
    this.scene.tweens.add({
      targets: this,
      angle: rotations * 360,
      duration: time * 1000,
      ease: 'Sine.easeInOut',
      onComplete: () => this.angle = 0
    });
  }

  enlarge()
  {
    this.scene.tweens.add({
      targets: this,
      scale: 1.2,
      duration: 300,
      ease: 'Sine.easeInOut'
    });
  }

  get number()
  {
    return this.numberText.text;
  }
}
