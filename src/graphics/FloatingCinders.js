import Phaser from 'phaser'
import Cinder from '../graphics/Cinder';

export default class FloatingCinders extends Phaser.GameObjects.Group
{
  constructor(scene)
  {
    super(scene);

    // for adding tweens for rising
    this.tweens = scene.tweens;

    // for math functions
    let rnd = Phaser.Math.RND;

    // create cinders, tween them to rise
    for(let i=0; i<90; i++)
    {
      let c = new Cinder({
        scene: this.scene,
        x: rnd.between(0, this.scene.sys.game.canvas.width),
        y: this.scene.sys.game.canvas.height+10,
      });

      // add to scene
      //this.scene.add(c);

      // add to this group
      this.add(c);

      // start upward movement
      this.tweens.add({
        targets: c,
        y: -10,
        duration: 5000 + rnd.frac() * 3000,
        delay: rnd.frac() * 15000,
        onComplete: () => this.repositionCinder(c),
      });

      c.timer = setTimeout( () => c.pulseIn(), rnd.frac() * 5000 );
    }
  }

  repositionCinder(c)
  {
    // for math functions
    let rnd = Phaser.Math.RND;

    c.x = rnd.between(0, this.scene.sys.game.canvas.width);
    c.y = this.scene.sys.game.canvas.height+10;

    this.tweens.add({
      targets: c,
      y: -10,
      duration: 10000,
      delay: rnd.between(0, 3) * 1000,
      onComplete: () => this.repositionCinder(c),
    });
  }


}
