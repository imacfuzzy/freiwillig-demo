import Phaser from 'phaser'

export default class StartCountdown extends Phaser.GameObjects.Container
{
  static get COUNTDOWN_END() { return "COUNTDOWN_END"; }
  static get COUNTDOWN_TICK() { return "COUNTDOWN_TICK"; }

  constructor(config)
  {
    super(config.scene);

    this.timer = null;

    // the current count
    this.countdown = 3;

    // the text object with the numbers for counting down
    this.countdownText = null;

    // where to display our countdownText
    this.displayX = 'displayX' in config ? config.displayX : this.scene.cameras.main.centerX;
    this.displayY = 'displayY' in config ? config.displayY : this.scene.cameras.main.centerY;

    this.scene.add.existing(this);
  }

  start()
  {
    this.timer = this.scene.time.addEvent({
      delay: 1300,
      callback: () => this.count(),
      repeat: 4
    });
  }

  count()
  {
    if( this.countdown ) {
      this.emit( StartCountdown.COUNTDOWN_CLICK );
      this.showCount( this.countdown );
    } else {
      this.emit( StartCountdown.COUNTDOWN_END );
      this.timer.remove();
      return;
    }
    this.countdown--;
  }

  showCount( num )
  {
    if( this.countdownText )
    {
      this.countdownText.destroy();
    }

    this.countdownText = this.scene.add.bitmapText(
      this.displayX,
      this.displayY,
      'fadetogr',
      num,
      80);
		this.countdownText.setCenterAlign();
		this.countdownText.setOrigin(0.5);

    this.add(this.countdownText);

    this.scene.tweens.add({
      targets: this.countdownText,
      fontSize: 90,
      alpha: 0,
      duration: 1100
    });
  }
}
