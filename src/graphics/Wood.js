import Phaser from 'phaser'

import GamePiece from '../graphics/GamePiece';

export default class Wood extends GamePiece
{
  constructor(config)
  {
    config.images = ["wood","fire"];
    config.type = "wood";
    config.clears = true;
    super(config);

    this.burning = false;

    // allow for wood to be created as burning
    if( config.burning === true ) {
      this.burning = true;
      this.clears = false;
      this.type = "fire";
      this.list[1].alpha = 1;
      this.first.alpha = 0;
    }
  }

  burn( delay = 0 )
  {
    this.burning = true;
    this.clears = false;
    this.type = "fire";

    this.scene.tweens.add({
      targets: this.first,
      alpha: 0,
      duration: this.transitionTime / 2,
      ease: this.transitionEase,
      delay: delay
    });
    this.scene.tweens.add({
      targets: this.list[1],
      alpha: 1,
      duration: this.transitionTime / 2,
      ease: this.transitionEase,
      delay: this.transitionTime / 2 + delay
    });
  }
}
