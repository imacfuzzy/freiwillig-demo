import Phaser from 'phaser'

import GamePiece from '../graphics/GamePiece';

export default class Water extends GamePiece
{
  constructor(config)
  {
    config.images = ["water","ice"];
    config.type = "water";
    config.clears = true;
    super(config);

    // move ice to the top
    this.bringToTop(this.list[1]);

    if( config.frozen ) {
      this.list[1].alpha = 1;
      this.type = "ice";
      this.frozen = true;
    }
  }

  freeze( delay = 0 )
  {
    this.frozen = true;
    this.type = "ice";

    this.scene.tweens.add({
      targets: this.list[1],
      alpha: 1,
      duration: this.transitionTime,
      ease: this.transitionEase,
      delay: delay
    });
  }

  unfreeze( delay = 0 )
  {
    this.frozen = false;
    this.type = "water";

    this.scene.tweens.add({
      targets: this.list[1],
      alpha: 0,
      duration: this.transitionTime,
      ease: this.transitionEase,
      delay: delay
    });
  }
}
