import Phaser from 'phaser'

export default class ArrowGuide extends Phaser.GameObjects.Container
{
  static get UP() { return "UP"; }
  static get LEFT() { return "LEFT"; }
  static get DOWN() { return "DOWN"; }
  static get RIGHT() { return "RIGHT"; }

  static get KEYPRESS() { return "KEYPRESS"; }

  constructor(config)
  {
    super(config.scene);

    // keep track of our highlight buttons
    this.highlights = {};

    let buttonWidth = 37;

    // bring in our arrows
    let up = this.scene.add.image( 0, 0, "arrow-up" );
    this.add(up);
    up.scale = buttonWidth / up.width;
    up.y = 0 - up.height * up.scale * 0.5;

    let down = this.scene.add.image( 0, 0, "arrow-down");
    this.add(down);
    down.scale = buttonWidth / down.width;
    down.y = down.height * down.scale * 0.5;

    let left = this.scene.add.image( 0, 0, "arrow-left");
    this.add(left);
    left.scale = buttonWidth / left.width;
    left.y = down.y;
    left.x = 0 - left.width * left.scale;

    let right = this.scene.add.image( 0, 0, "arrow-right");
    this.add(right);
    right.scale = buttonWidth / right.width;
    right.y = down.y;
    right.x = right.width * right.scale;

    // layer our highlight arrows on top with alpha 0
    this.highlights[ ArrowGuide.UP ] = this.scene.add.image( 0, 0, "arrow-highlight-up" );
    this.add(this.highlights[ ArrowGuide.UP ]);
    this.highlights[ ArrowGuide.UP ].scale = up.scale;
    this.highlights[ ArrowGuide.UP ].y = up.y;
    this.highlights[ ArrowGuide.UP ].alpha = 0;

    this.highlights[ ArrowGuide.DOWN ] = this.scene.add.image( 0, 0, "arrow-highlight-down" );
    this.add(this.highlights[ ArrowGuide.DOWN ]);
    this.highlights[ ArrowGuide.DOWN ].scale = down.scale;
    this.highlights[ ArrowGuide.DOWN ].y = down.y;
    this.highlights[ ArrowGuide.DOWN ].alpha = 0;

    this.highlights[ ArrowGuide.LEFT ] = this.scene.add.image( 0, 0, "arrow-highlight-left" );
    this.add(this.highlights[ ArrowGuide.LEFT ]);
    this.highlights[ ArrowGuide.LEFT ].scale = left.scale;
    this.highlights[ ArrowGuide.LEFT ].y = left.y;
    this.highlights[ ArrowGuide.LEFT ].x = left.x;
    this.highlights[ ArrowGuide.LEFT ].alpha = 0;

    this.highlights[ ArrowGuide.RIGHT ] = this.scene.add.image( 0, 0, "arrow-highlight-right" );
    this.add(this.highlights[ ArrowGuide.RIGHT ]);
    this.highlights[ ArrowGuide.RIGHT ].scale = right.scale;
    this.highlights[ ArrowGuide.RIGHT ].y = right.y;
    this.highlights[ ArrowGuide.RIGHT ].x = right.x;
    this.highlights[ ArrowGuide.RIGHT ].alpha = 0;

    this.scene.add.existing(this);
  }


  highlightArrow( img )
  {
    this.scene.tweens.add({
      targets: img,
      alpha: 1,
      duration: 100,
      ease: 'Quad.easeOut',
    });
    this.scene.tweens.add({
      targets: img,
      alpha: 0,
      duration: 1000,
      ease: 'Quad.easeOut',
      delay: 100,
    });
  }


  highlight(key)
  {
    this.highlightArrow( this.highlights[ key ] );
    this.emit( ArrowGuide.KEYPRESS );
  }

}
