import Phaser from 'phaser'

export default class EnterButton extends Phaser.GameObjects.Image
{

  constructor(scene, x, y)
  {
    super(scene, x, y, "enter" );
    this.pulseButtonIn();
  }

  pulseButtonIn()
  {
    this.scene.tweens.add({
      targets: this,
      scale: 0.4,
      ease: 'Bounce.easeOut',
      duration: 500,
      delay: 1000,
      onComplete: () => this.pulseButtonOut()
    });
  }

  pulseButtonOut()
  {
    this.scene.tweens.add({
      targets: this,
      scale: 0.8,
      ease: 'Bounce.easeOut',
      duration: 500,
      delay: 1000,
      onComplete: () => this.pulseButtonIn()
    });
  }

  fadeIn()
  {
    this.scene.tweens.add({
      targets: this,
      alpha: 1,
      duration: 100
    });
  }

  fadeOut()
  {
    this.scene.tweens.add({
      targets: this,
      alpha: 0,
      duration: 100
    });
  }

}
