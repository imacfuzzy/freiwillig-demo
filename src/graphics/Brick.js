import Phaser from 'phaser'

import GamePiece from '../graphics/GamePiece';

export default class Brick extends GamePiece
{
  constructor(config)
  {
    config.images = ["brick"];
    config.type = "brick";
    config.clears = true;
    super(config);
  }
}
