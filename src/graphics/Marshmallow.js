import Phaser from 'phaser'

import GamePiece from '../graphics/GamePiece';

export default class marshmallow extends GamePiece
{
  constructor(config)
  {
    config.images = ["marshmallow","toasted"];
    config.type = "marshmallow";
    super(config);

    this.toasted = false;
  }

  toast( delay = 0 )
  {
    this.toasted = true;

    this.scene.tweens.add({
      targets: this.first,
      alpha: 0,
      duration: this.transitionTime / 2,
      ease: this.transitionEase,
      delay: delay
    });
    this.scene.tweens.add({
      targets: this.list[1],
      alpha: 1,
      duration: this.transitionTime / 2,
      ease: this.transitionEase,
      delay: this.transitionTime / 2 + delay
    });
  }
}
