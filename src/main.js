import Phaser from 'phaser'

import TitleScene from './scenes/TitleScene'
import MainMenu from './scenes/MainMenu'

const config = {
	type: Phaser.AUTO,
	width: 600,
	height: 720,
	physics: {
		default: 'arcade',
		arcade: {
			gravity: { y: 200 },
		}
	},
	scene: [TitleScene, MainMenu]
	//scene: [TutorialScene]
}

export default new Phaser.Game(config)
