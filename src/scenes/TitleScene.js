import Phaser from 'phaser'

import SceneWithInput from '../scenes/SceneWithInput';
//import instance from '../scenes/MusicManager';
//import FloatingCinders from '../graphics/FloatingCinders';

export default class TitleScene extends SceneWithInput
{
	constructor()
	{
		super({
	    key: 'TitleScene',
			listenToClicks: true
    });
		this.animating = false;

		this.timeline = null;
	}


	preload()
  {
    // load any assets we will need
		this.load.audio("song", ["assets/soundfx/radio_sweep.ogg","assets/soundfx/radio_sweep.mp3"]);

		this.load.image("radio", "assets/images/radio.png");
		this.load.image("speech", "assets/images/speech-bubble.png");
		this.load.image("html5", "assets/images/logos/html5logo.png");
		this.load.image("phaser3", "assets/images/logos/phaser3logo.png");

		this.load.bitmapFont('title_sffedora', 'assets/fonts/SFFedora.png', 'assets/fonts/SFFedora.xml');
  }

  start()
  {
		if( this.animating ) return;
		this.animating = true;

		// play the radio sweep
		this.song.volume = 1;
		this.song.play();

		// run the tweens
		this.timeline = this.tweens.createTimeline();

		this.timeline.add({
			targets: this.radio,
			alpha: 1,
			duration: 500,
			ease: 'Quad.easeInOut',
		});
		this.timeline.add({
			targets: this.speech,
			alpha: 1,
			scale: 1,
			duration: 500,
			ease: 'Bounce.easeOut',
		});
		this.timeline.add({
			targets: this.text,
			alpha: 1,
			duration: 500,
			ease: 'Quad.easeOut',
		});
		this.timeline.add({
			targets: this.text,
			alpha:0,
			duration:500,
			delay:2000,
			ease: 'Quad.easeInOut',
		});
		this.timeline.add({
			targets: this.html5,
			alpha: 1,
			scale: 0.6,
			duration: 500,
			ease: 'Bounce.easeOut',
		});
		this.timeline.add({
			targets: this.html5,
			alpha:0,
			duration:500,
			delay:2000,
			ease: 'Quad.easeInOut',
		});
		this.timeline.add({
			targets: this.phaser3,
			alpha: 1,
			scale: 0.6,
			duration: 500,
			ease: 'Bounce.easeOut',
		});
		this.timeline.add({
			targets: this.phaser3,
			alpha:0,
			duration:500,
			delay:2000,
			ease: 'Quad.easeInOut',
			onComplete: () => this.fadeOut(),
		});
		this.timeline.play();
  }

	fadeOut()
	{
		this.timeline.stop();

		this.tweens.add({
			targets: this.song,
			volume: 0,
			duration:500,
		});
		this.tweens.add({
			targets: [this.speech, this.radio, this.text, this.html5, this.phaser3],
			alpha:0,
			duration:500,
			ease: 'Quad.easeInOut',
			onComplete: () => this.onCompleteHandler(),
		});
	}

	onCompleteHandler()
	{
		this.animating = false;
		this.song.stop();
		this.scene.stop();
		this.scene.start('MainMenu');
	}

	keyPress(value)
  {
		this.fadeOut();
  }

	clickHandler()
	{
		this.fadeOut();
	}

  create()
  {
    super.create();

		this.radio = this.add.sprite(this.cameras.main.centerX, this.cameras.main.centerY, 'radio');
		this.radio.y += this.radio.height/2;
		this.radio.alpha = 0;

		this.speech = this.add.sprite(this.cameras.main.centerX, this.cameras.main.centerY, 'speech');
		this.speech.y -= this.speech.height/2;
		this.speech.scale = 0.1;
		this.speech.alpha = 0;

    // create and position all game pieces
		this.text = this.add.bitmapText(this.cameras.main.centerX, this.speech.y - this.speech.height/4, 'title_sffedora', 'Unterwegs e.V.\npresents', 22);
		this.text.setCenterAlign();
		this.text.fontData.lineHeight = 70;
		this.text.setOrigin(0.5,0);
		this.text.alpha = 0;
		//console.log(this.text);

		this.html5 = this.add.image(this.cameras.main.centerX, this.cameras.main.centerY, 'html5');
		this.html5.scale = 0.1;
		this.html5.y = this.text.y + 20;
		this.html5.alpha = 0;

		this.phaser3 = this.add.image(this.cameras.main.centerX, this.cameras.main.centerY, 'phaser3');
		this.phaser3.scale = 0.1;
		this.phaser3.y = this.text.y + 20;
		this.phaser3.alpha = 0;

    //  The images will dispatch a 'clicked' event when they are clicked on
		this.input.on('pointerdown', () => this.clickHandler());

		this.song = this.sound.add("song");

		// start the animation
		this.start();
  }

}
