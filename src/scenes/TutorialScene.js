import Phaser from 'phaser'

import GameScene from '../scenes/GameScene';
import MainMenu from '../scenes/MainMenu';

import GamePiece from '../graphics/GamePiece';
import Wood from '../graphics/Wood';
import Water from '../graphics/Water';
import Bomb from '../graphics/Bomb';
import Marshmallow from '../graphics/Marshmallow';
import Brick from '../graphics/Brick';
import EnterButton from '../graphics/EnterButton';
import TypeTextBox from '../graphics/TypeTextBox';
import ArrowGuide from '../graphics/ArrowGuide';

import GameGrid from '../view/GameGrid';
import NextPieceDisplay from '../view/NextPieceDisplay';
import ThrowCountdownDisplay from '../view/ThrowCountdownDisplay';

import InputHandler from '../control/InputHandler';
import Options from '../control/Options';

import NextPieceGenerator from '../model/NextPieceGenerator';
import GameLogic from '../model/GameLogic';


/*
 * This class should be extended for any scene that will use GamePiece.
 */
export default class TutorialScene extends GameScene
{
  constructor()
  {
    super('TutorialScene');
    this.bg = "assets/images/backgrounds/academy.jpg";
    this.musicAsset = "assets/music/David_Szesztay_-_Traveller.mp3";

    // the box where we will be showing text
    this.typeTextBox = null;

    // the pulsing enter button to show that enter can be pressed
    this.enterButton = null;

    // an associative array of the assets we add
    this.assets = {};

    // toggle for responding to input
    this.listeningForEnter = false;

    // the arrow display for tips
    this.arrowGuide = null;

    // the sound to play when a virtual key is pressed
    this.keyPressSound = null;

    // the long array of calls we progress through
    this.calls = [
      {
        func: () => this.time.delayedCall(
          1000,
          () => this.loadNextCall()
        )
      },
      {
        asset: "lionel",
        animate: "enter"
      },
      {
        content: [
        "Welcome to Freiwillig, volunteer! My name is Lionel Leiterwagen. Press 'Enter' when you see that button in the bottom right to keep going.",
        "I lead our team of brave, unpaid firefighters. Before we jump into the action, let me show you how things work here at the fire department.",
        ],
      },
      {
        asset: "lionel",
        animate: "exit"
      },
      {
        func: () => {
          this.tweens.add({
            targets: this.gameGrid,
            alpha: 1,
            duration: 500,
            onComplete: () => this.loadNextCall()
          });
        }
      },
      {
        content: [
        "This is the game field. The game field is where you will be doing your work.",
        "Your work? Use the arrow keys to move and rotate the falling pieces to make combos and rack up sweet, sweet points.",
        ],
      },
      {
        func: () => {
          this.tweens.add({
            targets: this.nextPieceDisplay,
            alpha: 1,
            duration: 500,
            onComplete: () => this.loadNextCall()
          });
        }
      },
      {
        content: [
        "This is the preview field. Here you can see the piece that will be falling next.",
        ]
      },
      {
        func: () => {
          this.nextPieceDisplay.loadNewPieces( "wood", "brick" );
          this.time.delayedCall( 1000, () => this.loadNextCall() );
          this.tweens.add({
            targets: this.arrowGuide,
            alpha: 1,
            duration: 500
          });
        }
      },
      {
        content: [
        "Looks like we have a wood piece and a brick piece that are about to fall.",
        ],
      },
      {
        func: () => {
          this.shiftPiece( "water", "brick" );
          this.time.delayedCall( 1050, () => this.down() );
          this.time.delayedCall( 1650, () => this.down() );
        }
      },
      {
        content: [
        "Pieces will fall automatically - or you can use the down arrow key to give gravity an assist.",
        ],
      },
      {
        func: () => {
          this.shiftPiece( "brick", "brick" );
          this.time.delayedCall( 900, () => this.right() );
          this.time.delayedCall( 1400, () => this.right() );
          this.time.delayedCall( 1900, () => this.up() );
          this.time.delayedCall( 2400, () => this.up() );
        }
      },
      {
        content: [
        "The left and right arrows move side to side. The up arrow rotates the piece. Let's put the bricks together.",
        ],
      },
      {
        func: () => {
          this.shiftPiece( "fire", "brick" );
          this.time.delayedCall( 900, () => this.right() );
          this.time.delayedCall( 1900, () => this.up() );
        }
      },
      {
        content: [
        "We can clear brick, wood, water, and ice pieces if we connect 5 together. Let's make that happen.",
        ],
      },
      {
        func: () => {
          this.shiftPiece( "wood", "water" );
          this.time.delayedCall( 900, () => this.up() );
          this.time.delayedCall( 1900, () => this.up() );
          this.time.delayedCall( 2400, () => this.up() );
        }
      },
      {
        content: [
        "It was just a matter of time - here comes fire. Be careful! Fire spreads if it gets near wood.",
        ],
      },
      {
        func: () => {
          this.shiftPiece( "ice", "ice" );
          this.time.delayedCall( 900, () => this.up() );
          this.time.delayedCall( 1900, () => this.down() );
        }
      },
      {
        content: [
        "Our only weapon against fire is water. Water will extinguish fire if it is right next to it.",
        ],
      },
      {
        func: () => {
          this.shiftPiece( "marshmallow", "wood" );
          this.time.delayedCall( 900, () => this.right() );
        }
      },
      {
        content: [
        "Ice is doubly effective against fire. Ice melts when next to fire - and it freezes water left sitting next to it.",
        ],
      },
      {
        func: () => {
          this.shiftPiece( "fire", "ice" );
          this.time.delayedCall( 900, () => this.down() );
          this.time.delayedCall( 1900, () => this.down() );
        }
      },
      {
        content: [
        "Here is an interesting piece - the marshmallow. Marshmallows cannot be cleared - only toasted by fire.",
        ],
      },
      {
        func: () => {
          this.shiftPiece( "bomb", "marshmallow" );
          this.time.delayedCall( 900, () => this.down() );
          this.time.delayedCall( 1900, () => this.down() );
        }
      },
      {
        content: [
        "Pay attention - marshmallows will always toast before water has any effect on the fire.",
        "Did you see that medal appear? The bigger your combo's, the bigger the points! Stack your pieces to make big chain reactions."
        ],
      },
      {
        func: () => {
          this.shiftPiece( "fire", "ice" );
          this.time.delayedCall( 900, () => this.down() );
          this.time.delayedCall( 1400, () => this.up() );
          this.time.delayedCall( 1900, () => this.up() );
          this.time.delayedCall( 2300, () => this.up() );
          this.time.delayedCall( 2900, () => this.right() );
        }
      },
      {
        content: [
        "Here is the most flammable piece - the bomb. If the bomb comes into contact with fire, it explodes!",
        ],
      },
      {
        func: () => {
          this.shiftPiece( "fire", "brick" );
          this.time.delayedCall( 900, () => this.down() );
          this.time.delayedCall( 1500, () => this.up() );
          this.time.delayedCall( 2300, () => this.right() );
        }
      },
      {
        content: [
        "An exploding bomb will take out all the pieces around it. This can be useful - if you use it right!",
        ],
      },
      {
        func: () => {
          this.shiftPiece( "fire", "brick" );
          this.time.delayedCall( 900, () => this.down() );
          this.time.delayedCall( 1500, () => this.up() );
          this.time.delayedCall( 2300, () => this.right() );
        }
      },
      {
        func: () => {
          this.tweens.add({
            targets: this.throwCountdownDisplay,
            alpha: 1,
            duration: 500,
            onComplete: () => this.loadNextCall()
          });
          this.tweens.add({
            targets: this.arrowGuide,
            alpha: 0,
            duration: 500,
            onComplete: () => this.loadNextCall()
          });
        }
      },
      {
        content: [
        "This is the countdown. The numbers display how many pieces you have until a big wave hits!",
        ],
      },
      {
        func: () => {
          this.throwCountdownDisplay.show(2);
          this.time.delayedCall( 1100, () => this.throwCountdownDisplay.show(1) );
          this.time.delayedCall( 2200, () => this.throwCountdownDisplay.show(0) );
          this.time.delayedCall( 2300, () => this.loadNextCall() );
        }
      },
      {
        content: [
        "Keep a close eye on this - be prepared to react to whatever may fall from above!",
        ],
      },
      // perform a throw
      // reaction check
      {
        func: () => {
          this.time.delayedCall(
  					this.gameGrid.addThrow(
              this.generateThrowPieces( this.throwManager.throw_dropJunk( 6, 2 ) )
            ) + GameGrid.TIME_BUFFER,
  					() => this.gameGrid.check()
  				);
        }
      },
      {
        content: [
        "The best of the freiwillige Feuerwehr can use these big waves to their advantage.",
        ],
      },
      // show lionel
      {
        asset: "lionel",
        animate: "enter"
      },
      {
        content: [
        "Alright, that completes your basic training. Good luck! Remember: we don't get paid, and we earn every cent!"
        ],
      },
      // exit tutorial
      {
        func: () => {
          this.returnToMainMenu();
        }
      },
    ];
    this.callsCounter = 0;
  }


  down()
  {
    this.gameGrid.dropFallingPiece();
    this.arrowGuide.highlight( ArrowGuide.DOWN );
  }

  up()
  {
    this.gameGrid.rotateFallingPiece();
    this.arrowGuide.highlight( ArrowGuide.UP );
  }

  left()
  {
    this.gameGrid.moveFallingPieceLeft();
    this.arrowGuide.highlight( ArrowGuide.LEFT );
  }

  right()
  {
    this.gameGrid.moveFallingPieceRight();
    this.arrowGuide.highlight( ArrowGuide.RIGHT );
  }


  defineInputHandler()
  {
    this.inputHandler = new InputHandler();
    this.inputHandler.on(
      InputHandler.ENTER,
      () => this.enterPressed()
    );
  }


  defineGameGrid()
  {
    this.gameGrid = new GameGrid({scene:this, gridWidth:6, gridHeight: 8});
    this.gameGrid.alpha = 0;
    //this.gameGrid.x += 160;
    this.gameGrid.y += 70;

    this.gameGrid.on(GameGrid.GAME_NEWROUND, (data) => this.logic(data) );
  }

  defineComboCounter()
  {
    super.defineComboCounter();
    this.comboCounter.displayY += 100;
  }

  defineNextPieceDisplay()
  {
    this.nextPieceDisplay = new NextPieceDisplay({scene:this});

    this.nextPieceDisplay.y = this.gameGrid.y - this.gameGrid.height * 0.5 - 10 - this.nextPieceDisplay.height * 0.5;;
    this.nextPieceDisplay.x = this.gameGrid.x;

    this.nextPieceDisplay.alpha = 0;
  }

  defineThrowCountdownDisplay()
  {
    this.throwCountdownDisplay = new ThrowCountdownDisplay({scene:this, maxBadges:6});
    this.throwCountdownDisplay.x = this.gameGrid.x;
    this.throwCountdownDisplay.y = this.gameGrid.y + this.gameGrid.height * 0.5 + 10 + this.throwCountdownDisplay.height * 0.5;

    this.throwCountdownDisplay.show(3);
    this.throwCountdownDisplay.alpha = 0;
  }


  preload()
  {
    super.preload();

    // assets for characters
		this.load.image("lionel", "assets/images/characters/heroes/ll.png");

    // assets for TypeTextBox
    this.load.audio("dialogue_text", ["assets/soundfx/dialogue_text.mp3"]);

    // assets for arrow tips
    this.load.image("arrow-up", "assets/images/arrow-key-up.png");
    this.load.image("arrow-left", "assets/images/arrow-key-left.png");
    this.load.image("arrow-down", "assets/images/arrow-key-down.png");
    this.load.image("arrow-right", "assets/images/arrow-key-right.png");

    this.load.image("arrow-highlight-up", "assets/images/arrow-highlight-up.png");
    this.load.image("arrow-highlight-left", "assets/images/arrow-highlight-left.png");
    this.load.image("arrow-highlight-down", "assets/images/arrow-highlight-down.png");
    this.load.image("arrow-highlight-right", "assets/images/arrow-highlight-right.png");

    this.load.audio("keypress", ["assets/soundfx/tutorial_keypress.mp3"]);
  }


  create()
  {
    super.create();

    // the music for the tutorial is relatively loud, let's make it quieter
    this.music.setVolume(Options.musicVolume / 2);

    this.soundEffects.dialogue_text = this.sound.add("dialogue_text");
    this.soundEffects.dialogue_text.setVolume(Options.soundfxVolume);
    this.soundEffects.dialogue_text.setLoop(true);

    this.arrowGuide = new ArrowGuide({scene:this});
    this.arrowGuide.x = this.cameras.main.centerX - this.sys.game.canvas.width * 0.5 + 85;
    this.arrowGuide.y = this.sys.game.canvas.height - 80;
    this.arrowGuide.alpha = 0;

    this.keyPressSound = this.sound.add("keypress");
    this.keyPressSound.setVolume(Options.soundfxVolume);
    this.arrowGuide.on( ArrowGuide.KEYPRESS, () => this.keyPressSound.play() );

    this.enterButton = new EnterButton(
      this,
      this.sys.game.canvas.width - 60,
      this.sys.game.canvas.height - 60
    );
    this.enterButton.alpha = 0;
    this.add.existing( this.enterButton );

    this.loadNextCall();
  }


  loadNextCall()
  {
    if( this.callsCounter >= this.calls.length )
    {
      console.log("All done with calls!");
      return false;
    }
    const c = this.calls[this.callsCounter++];
    if( 'func' in c )
    {
      c.func.call();
    }
    else if( 'content' in c )
    {
      this.showTypeTextBox({
        content: c.content,
        shieldWidth: this.sys.game.canvas.width - 20,
        shieldHeight: 127,
        displayFontSize: 20,
        displayX: this.cameras.main.centerX,
        displayY: 147 / 2 + 10,
      });
    }
    else if( 'asset' in c )
    {
      this.animate( c )
    }
  }

  showTypeTextBox(obj)
  {
    if( this.typeTextBox !== null ) console.error("typeTextBox should be null in showTypeTextBox()");

    obj.scene = this;
    this.typeTextBox = new TypeTextBox( obj );
    this.typeTextBox.on(
      TypeTextBox.SHOWING_TEXT,
      () => this.soundEffects.dialogue_text.play()
    );
    this.typeTextBox.on(
      TypeTextBox.WAITING_FOR_ENTER,
      () => {
        this.soundEffects.dialogue_text.stop();
        this.showEnterButton();
      }
    );
    this.typeTextBox.on(
      TypeTextBox.DONE,
      () => {
        this.typeTextBox.destroy();
        this.typeTextBox = null;
        this.loadNextCall();
      }
    );

    this.typeTextBox.show();
  }

  animate(obj)
  {
    if( !( obj.asset in this.assets ) )
    {
      this.assets[obj.asset] = this.add.image( -2000, -2000, obj.asset );
    }
    switch( obj.animate )
    {
      case "enter":
        this.assets[obj.asset].x = 0 - this.assets[obj.asset].width * 0.5;
        this.assets[obj.asset].y = this.sys.game.canvas.height - this.assets[obj.asset].height  * 0.5;
        this.tweens.add({
          targets: this.assets[obj.asset],
          duration: 300,
          x: this.assets[obj.asset].width * 0.5,
          ease: 'Quad.easeInOut',
          onComplete: () => this.loadNextCall()
        });
        break;
      case "exit":
        this.tweens.add({
          targets: this.assets[obj.asset],
          duration: 300,
          x: 0 - this.assets[obj.asset].width * 0.5,
          ease: 'Quad.easeInOut',
          onComplete: () => this.loadNextCall()
        });
        break;
    }
  }

  showEnterButton()
  {
    this.enterButton.fadeIn();
    this.listeningForEnter = true;
  }

  enterPressed()
  {
    if( !this.listeningForEnter )
    {
      if( this.typeTextBox !== null ) this.typeTextBox.skip();
      return;
    }

    this.enterButton.fadeOut();
    this.listeningForEnter = false;

    if( this.typeTextBox !== null )
    {
      this.typeTextBox.next();
    }
    else
    {
      this.loadNextCall();
    }
  }


  shiftPiece( type1, type2 )
  {
    this.gameGrid.nextPieceArray = [
      this.generateGamePiece( this.nextPieceDisplay.getPieceTypes(0) ),
      this.generateGamePiece( this.nextPieceDisplay.getPieceTypes(1) ),
    ];
    this.gameGrid.addPiece();

    this.nextPieceDisplay.loadNewPieces( type1, type2 );
  }


  logic(data)
  {
    let logic = new GameLogic( data.grid );
    let reactions = logic.reactionCheck();

    if( this.gameGrid === null ) return;

    if( reactions.length < 1 )
    {
      // there are no more reactions to handle, tutorial can proceed
      this.time.delayedCall( GameGrid.TIME_BUFFER, () => this.loadNextCall() );

      // reset our ComboCounter
      if( this.comboCounter !== null ) this.comboCounter.reset();

    }
    else {
      this.time.delayedCall(
        this.handleReactions( reactions ) + GameGrid.TIME_BUFFER,
        () => this.gameGrid.check()
      );
    }
  }

  returnToMainMenu()
  {
    this.cameras.main.fadeOut(500);
    this.tweens.add({
      targets: this.music,
      volume: 0,
      duration: 1000,
      onComplete: () => {
        this.music.stop();
        this.time.delayedCall(500, () => {
          this.scene.stop();
          this.scene.remove('TutorialScene');
          this.scene.add('MainMenu', MainMenu);
          this.scene.start('MainMenu');
        });
      }
    });
  }

}
