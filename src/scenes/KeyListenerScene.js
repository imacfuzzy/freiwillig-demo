import Phaser from 'phaser'

export default class KeyListenerScene extends Phaser.Scene
{
	constructor()
	{
		super('key-listener-scene')
	}

	preload()
  {

  }

  create()
  {
    this.input.keyboard.on('keydown', () => this.keyPress(event.key) );
  }

  keyPress(value)
  {
    console.log("KeyListenerScene keyPress: " + value);
  }

}
