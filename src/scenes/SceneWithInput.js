import Phaser from 'phaser'

export default class SceneWithInput extends Phaser.Scene
{

	constructor(config)
	{
		super(config);

		/*
		// for when the browser view changes
		document.addEventListener("visibilitychange", () => {
	    if (document.visibilityState === "visible"){
	        this.scene.start();
	    }
	    if (document.visibilityState === "hidden"){
	        this.scene.stop();
	    }
		});
		*/

    if( 'listenToClicks' in config )
		{
			this.listenToClicks = config.listenToClicks;
		}

		if( 'listenToKeyboard' in config )
		{
    	this.listenToKeyboard = config.listenToKeyboard;
		}
	}

	preload()
  {

  }

  create()
  {
    if( this.listenToClicks )
    {
      this.input.on(
        'gameobjectup',
        function (pointer, gameObject)
        {
          //console.log("SceneWithInput click detected");
          gameObject.emit('clicked', gameObject);
        },
        this);
				/*
			this.input.on(
				'pointerover',
				function( pointer, gameObject )
				{
					console.log("SceneWithInput pointerover detected", gameObject);
					//gameObject.emit('pointerover', gameObject);
					//this.mouseOver(gameObject);
				}
				//() => this.mouseOver( this.gameObject )
			)
			*/
    }

    if( this.listenToKeyboard )
    {
       this.input.keyboard.on('keydown', () => this.keyPress(event.key) );
    }
  }

  keyPress(value)
  {
    //console.log("SceneWithInput keyPress: " + value);
  }

	mouseOver(gameObject)
	{
		//console.log("SceneWithInput mouseOver: " + gameObject);
	}

	/*
	close()
	{
		if( this.listenToKeyboard )
    {
		  this.input.keyboard.off('keydown', () => this.keyPress(event.key) );
		}
		if( this.listenToClicks )
    {
      this.input.off(
        'gameobjectup',
        function (pointer, gameObject)
        {
          //console.log("SceneWithInput click detected");
          gameObject.emit('clicked', gameObject);
        },
        this);
    }
	}
	*/

	restart()
	{
		this.registry.destroy();
		this.events.off();
		this.scene.restart();
	}

}
