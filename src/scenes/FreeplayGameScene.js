import Phaser from 'phaser'

import GameScene from '../scenes/GameScene';
import MainMenu from '../scenes/MainMenu';

import ClearHandler from '../model/ClearHandler';

import Options from '../control/Options';

import Cinder from '../graphics/Cinder';

import GameGrid from '../view/GameGrid';
import NextPieceDisplay from '../view/NextPieceDisplay';
import ThrowCountdownDisplay from '../view/ThrowCountdownDisplay';
import ComboCounter from '../view/ComboCounter';
import ScoreDisplay from '../view/ScoreDisplay';


export default class FreeplayGameScene extends GameScene
{
  static get margin() { return 5; }

  constructor()
  {
    super('FreeplayGameScene');
    this.musicAsset = "assets/music/Freiwillig2_mp3.mp3";
    this.bg = "assets/images/backgrounds/flamebg.jpg";
  }

  defineGameGrid()
  {
    this.gameGrid = new GameGrid({scene:this});
  }


  defineNextPieceDisplay()
	{
    this.nextPieceDisplay = new NextPieceDisplay({scene:this});

    this.nextPieceDisplay.y = this.gameGrid.y - this.gameGrid.height * 0.5 - FreeplayGameScene.margin - this.nextPieceDisplay.height * 0.5;
    this.nextPieceDisplay.x = this.gameGrid.x - this.gameGrid.width * 0.5 + this.nextPieceDisplay.width * 0.5;

    this.nextPieceDisplay.loadNewPieces(this.getNextPieceType(),this.getNextPieceType());
	}

  defineThrowCountdownDisplay()
  {
    this.throwCountdownDisplay = new ThrowCountdownDisplay({scene:this, maxBadges:6});

    /*
    this.throwCountdownDisplay.x = this.cameras.main.centerX;
    this.throwCountdownDisplay.y = 500;
    */
    this.throwCountdownDisplay.x = this.cameras.main.centerX;
    this.throwCountdownDisplay.y = this.gameGrid.y + this.gameGrid.height * 0.5 + FreeplayGameScene.margin + this.throwCountdownDisplay.height * 0.5;

  }

  defineInfoDisplay()
  {
    this.infoDisplay = new ScoreDisplay({scene:this});

    /*
    this.infoDisplay.y = this.cameras.main.centerY;
    this.infoDisplay.x = 650;
    */
    this.infoDisplay.y = this.gameGrid.y - this.gameGrid.height * 0.5 - FreeplayGameScene.margin - this.infoDisplay.height * 0.5;
    this.infoDisplay.x = this.gameGrid.x + this.gameGrid.width * 0.5 - this.infoDisplay.width * 0.5;
  }


  defineClearHandler()
  {
    this.clearHandler = new ClearHandler();
  }


  preload()
  {
    super.preload();

		this.load.audio("throw_fire", ["assets/soundfx/throw_fire.mp3"]);
    this.load.audio("throw_quake", ["assets/soundfx/throw_quake.mp3"]);

    // image necessary for the floating cinders
    this.load.image("cinder", "assets/images/cinder.png");
  }

  create()
  {
    super.create();

    // the music for the tutorial is relatively loud, let's make it quieter
    this.music.setVolume(Options.musicVolume / 3);

    this.soundEffects.throw_fire = this.sound.add("throw_fire");
    this.soundEffects.throw_fire.setVolume(Options.soundfxVolume);

    this.soundEffects.throw_quake = this.sound.add("throw_quake");
    this.soundEffects.throw_quake.setVolume(Options.soundfxVolume);

    //console.log( this.soundEffects );

    // countdown to start
		this.startCountdown.start();
  }

  start()
  {
    super.start();
    this.shake();
  }

  handleThrowWarning()
  {
    this.time.delayedCall(
      1000,
      () => this.shake()
    );
  }

  shake()
  {
    // camera shake
    this.cameras.main.shake(800, 0.01);

    // sound effects
    this.soundEffects.throw_fire.play();
    this.soundEffects.throw_quake.play();

    // float some cinders from the bottom up

    // for math functions
    let rnd = Phaser.Math.RND;

    // create cinders, tween them to rise
    for(let i=0; i<90; i++)
    {
      let c = new Cinder({
        scene: this,
        x: rnd.between(0, this.sys.game.canvas.width),
        y: this.sys.game.canvas.height+10,
      });

      this.add.existing(c);

      // start upward movement
      this.tweens.add({
        targets: c,
        y: -10,
        duration: 4000 + rnd.frac() * 3000,
        delay: rnd.frac() * 2000,
        onComplete: () => c.destroy(),
      });

      c.timer = setTimeout( () => c.pulseIn(), rnd.frac() * 5000 );
    }
  }

  handleReactions( reactions )
  {
    let a = Math.floor(this.clearHandler.score / 500);
    let x = Math.floor(this.clearHandler.score / 1000);
    let z = Math.floor(this.clearHandler.score / 4000);

    let time = super.handleReactions( reactions );

    let b = Math.floor(this.clearHandler.score / 500);
    let y = Math.floor(this.clearHandler.score / 1000);
    let zz = Math.floor(this.clearHandler.score / 4000);

    if( b > a )
    {
      // quicken the autodrop timer
      //console.log( "autodrop speeding up" );
      this.gameGrid.speedUpAutoDropTimer();
    }
    if( y > x )
    {
      // up the throw difficulty
      //console.log( "upping throw difficulty" );
      this.throwManager.upDifficulty();
    }
    if( zz > z )
    {
      // throws come sooner
      //console.log( "decreasing throw interval" );
      this.throwManager.decreaseInterval();
    }

    return time;
  }

  gameOverConfirmed()
  {
    super.gameOverConfirmed();
    this.cameras.main.fadeOut(500);
    this.time.delayedCall( 500, () => {
      this.scene.stop();
      this.scene.remove('FreeplayGameScene');
      this.scene.add('MainMenu', MainMenu);
      this.scene.start('MainMenu');
    });
  }

}
