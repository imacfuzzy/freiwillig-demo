import Phaser from 'phaser'
import TextMenu from '../scenes/TextMenu';
import FloatingCinders from '../graphics/FloatingCinders';

import TutorialScene from '../scenes/TutorialScene';
import FreeplayGameScene from '../scenes/FreeplayGameScene';
import OptionsMenu from '../scenes/OptionsMenu';

export default class MainMenu extends TextMenu
{
  constructor()
  {
    super({
      key: 'MainMenu',
      menu: [
        {title: "DEMO", function: () => this.startDemo() },
        {title: "TUTORIAL", function: ()=> this.startTutorial() },
        {title: "OPTIONS", function: ()=> this.startOptions() },
      ],
      title: "Freiwillig Demo",
      sound: {
        switch: "assets/soundfx/menu_arrow.mp3",
        select: "assets/soundfx/menu_select.mp3",
        music: "assets/music/Hollywood Trailer.mp3"
      },
      listenToClicks: true,
      listenToKeyboard: true
    });
  }

  preload()
  {
    super.preload();

    // load the background
    this.load.image("flamebg", "assets/images/backgrounds/firefighter.jpg");

    // load the assets for our tip at the bottom of the screen
		this.load.bitmapFont('vcrosdmn', 'assets/fonts/vcrosdmono.png', 'assets/fonts/vcrosdmono.xml');

    this.load.image("arrow-up", "assets/images/arrow-key-up.png");
    this.load.image("arrow-down", "assets/images/arrow-key-down.png");
    this.load.image("enter", "assets/images/enter.png");

    // image necessary for the floating cinders
    this.load.image("cinder", "assets/images/cinder.png");
  }

  create()
  {
    // add fire background
		this.firebg = this.add.image(this.cameras.main.centerX, this.cameras.main.centerY, 'flamebg');
		this.firebg.scale = this.sys.game.canvas.height / this.firebg.height;

    // add our tooltip
    let tooltip = this.add.bitmapText(
      this.cameras.main.centerX,
      this.sys.game.canvas.height - 50,
      'vcrosdmn',
      "Use    and    to navigate and    to confirm.",
      17
    );
    tooltip.setOrigin(0.5);
    let up = this.add.image( tooltip.x - 170, tooltip.y, "arrow-up" );
    up.scale = 26 / up.height;
    let down = this.add.image( tooltip.x - 98, tooltip.y, "arrow-down" );
    down.scale = 26 / down.height;
    let enter = this.add.image( tooltip.x + 92, tooltip.y, "enter" );
    enter.scale = 26 / enter.height;

    // add a cinder group
		this.cinders = new FloatingCinders(this);

    super.create();

    //this.menu_music.setVolume(0.5);

    this.cameras.main.fadeIn(500, 0, 0, 0);
  }

  switchScene( key, _class )
  {
    this.cameras.main.fadeOut(500);
    this.tweens.add({
      targets: this.menu_music,
      volume: 0,
      duration: 1000,
      onComplete: () => {
        this.menu_music.stop();
        this.time.delayedCall(500, () => {
          this.scene.stop();
          this.scene.remove('MainMenu');
          this.scene.add(key, _class);
          this.scene.start(key);
        });
      }
    });
  }

  startDemo()
  {
    this.switchScene('FreeplayGameScene', FreeplayGameScene );
  }

  startTutorial()
  {
    this.switchScene('TutorialScene', TutorialScene);
  }

  startOptions()
  {
    this.switchScene('OptionsMenu', OptionsMenu);
  }

}
