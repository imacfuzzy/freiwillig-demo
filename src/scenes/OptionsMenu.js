import Phaser from 'phaser'

import Options from '../control/Options';

import TextMenu from '../scenes/TextMenu';
import MainMenu from '../scenes/MainMenu';

export default class OptionsMenu extends TextMenu
{
  constructor()
  {
    super({
      key: 'OptionsMenu',
      title: "OPTIONS",
      menu: [
        {title: "MUSIC VOLUME: ", function: () => this.musicVolume() },
        {title: "SOUNDFX VOLUME: ", function: ()=> this.soundfxVolume() },
        {title: "BACK TO MAIN MENU", function: ()=> this.returnToMainMenu() },
      ],
      sound: {
        switch: "assets/soundfx/menu_arrow.mp3",
        select: "assets/soundfx/menu_select.mp3",
        music: "assets/music/Concept1_Menu.mp3"
      },
      listenToClicks: true,
      listenToKeyboard: true
    });

    // replace the default font sizes
    this.fontSizes = {
      selected: 52,
      notSelected: 32
    };
  }

  preload()
  {
    super.preload();

    // load the background
    this.load.image("firebg", "assets/images/backgrounds/fire-bg.gif");
  }

  create()
  {
    let bg = this.add.image(this.cameras.main.centerX, this.cameras.main.centerY, 'firebg');
		bg.scale = this.sys.game.canvas.width / bg.width;

    super.create();

    this.buttons[0].text += " " + Options.musicVolume;
    this.buttons[1].text += " " + Options.soundfxVolume;

    this.cross_dist_to_text = 100;
  }

  musicVolume()
  {
    Options.musicVolume -= 0.1;
    Options.musicVolume = parseFloat(Options.musicVolume.toFixed(1));
    if( Options.musicVolume < 0 ) Options.musicVolume = 1;

    let v = Options.musicVolume;
    if( Options.musicVolume === 1 ) v += ".0";

    this.buttons[0].text = this.buttons[0].text.substring( 0, this.buttons[0].text.length - 4 ) + " " + v;

    this.menu_music.setVolume(Options.musicVolume);
  }

  soundfxVolume()
  {
    Options.soundfxVolume -= 0.1;
    Options.soundfxVolume = parseFloat(Options.soundfxVolume.toFixed(1));
    if( Options.soundfxVolume < 0 ) Options.soundfxVolume = 1;

    let v = Options.soundfxVolume;
    if( Options.soundfxVolume === 1 ) v += ".0";

    this.buttons[1].text = this.buttons[1].text.substring( 0, this.buttons[1].text.length - 4 ) + " " + v;

    if( this._select ) this._select.setVolume( Options.soundfxVolume );
    if( this._switch ) this._switch.setVolume( Options.soundfxVolume );
  }

  returnToMainMenu()
  {
    this.cameras.main.fadeOut(500);
    this.tweens.add({
      targets: this.menu_music,
      volume: 0,
      duration: 1000,
      onComplete: () => {
        this.menu_music.stop();
        this.time.delayedCall(500, () => {
          this.scene.stop();
          this.scene.remove('OptionsMenu');
          this.scene.add('MainMenu', MainMenu);
          this.scene.start('MainMenu');
        });
      }
    });
  }
}
