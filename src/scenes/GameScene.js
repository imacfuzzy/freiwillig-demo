import Phaser from 'phaser'

import SceneWithInput from '../scenes/SceneWithInput';

import GamePiece from '../graphics/GamePiece';
import Wood from '../graphics/Wood';
import Water from '../graphics/Water';
import Bomb from '../graphics/Bomb';
import Marshmallow from '../graphics/Marshmallow';
import Brick from '../graphics/Brick';
import StartCountdown from '../graphics/StartCountdown';

import ParticleManager from '../view/ParticleManager';
import GameGrid from '../view/GameGrid';
import ComboCounter from '../view/ComboCounter';
import GameOverDisplay from '../view/GameOverDisplay';

import InputHandler from '../control/InputHandler';
import Options from '../control/Options';

import NextPieceGenerator from '../model/NextPieceGenerator';
import GameLogic from '../model/GameLogic';
import ClearHandler from '../model/ClearHandler';
import ThrowManager from '../model/ThrowManager';

import Gravity from '../model/reactions/Gravity';
import Clear from '../model/reactions/Clear';
import Freeze from '../model/reactions/Freeze';
import Extinguish from '../model/reactions/Extinguish';
import Burn from '../model/reactions/Burn';
import Toast from '../model/reactions/Toast';
import Explode from '../model/reactions/Explode';

/*
 * This class should be extended for any scene that will use GamePiece.
 */
export default class GameScene extends SceneWithInput
{
	constructor( key = null )
	{
		if( key === null ) key = 'GameScene';
		super({
	    key: key,
			listenToClicks: false,
      listenToKeyboard: true
    });

		// generate a unique id for the assets of this scene
		this.key = Math.random().toString(36).substring(7) + "_" + key;

		 /********
		 * MODEL *
		 ********/

		// each GameScene needs a way of getting new GamePieces
		this.nextPieceGenerator = null;
		this.defineNextPieceGenerator();

		// each GameScene needs a way of managing throws
		this.throwManager = null;
		this.defineThrowManager();

		// each GameScene should have a way of handling clears made
		this.clearHandler = null;
		this.defineClearHandler();


		/*******
		* VIEW *
		*******/

		// shows the countdown to starting
		this.startCountdown = null;

		// shows a game over message
		this.gameOverDisplay = null;

		// shows the next piece to fall
		this.nextPieceDisplay = null;

		// shows information regarding the state of the game
		this.infoDisplay = null;

		// the game field with falling and stacked game pieces
		this.gameGrid = null;

		// the display for countdown to the next throw
		this.throwCountdownDisplay = null;

		// the combo medal display
		this.comboCounter = null;

		// the picture that sits in the background
		this.bg = null;

		// the glows we illuminate on receiving input
		this.glows = {
			up: null,
			down: null,
			left: null,
			right: null
		}


		/**********
		* CONTROL *
		**********/

		// handles keyboard/mouse input
		this.inputHandler = null;


		/********
		* MUSIC *
		********/

		// this can be changed in subclasses create() method to load other music
		this.musicAsset = "";

		// the phaser music object
		this.music = null;

		// plays when the game is over
		this.gameOverMusic = null;

	}

	preload()
  {
    // assets for GamePiece
		this.load.image("wood",         "assets/images/wood.png");
    this.load.image("water",        "assets/images/water.png");
    this.load.image("ice",          "assets/images/ice-overlay.png");
    this.load.image("fire",         "assets/images/fire.png");
    this.load.image("marshmallow",  "assets/images/marshmallow.png");
    this.load.image("toasted",      "assets/images/marshmallow-surprise.png");
    this.load.image("brick",        "assets/images/brick.png");
		this.load.image("bomb",         "assets/images/bomb.png");
    this.load.image("redbomb",      "assets/images/bomb-red.png");

		// assets for ComboCounter
    this.load.image("medal1",      "assets/images/medal1.png");
    this.load.image("medal2",      "assets/images/medal2.png");
    this.load.image("medal3",      "assets/images/medal3.png");

    // assets for particle effects
    this.load.image("smoke",        "assets/images/smoke.png");
    this.load.image("splash",       "assets/images/splash.png");
    this.load.image("star",         "assets/images/star.png");
    this.load.image("snowflake",    "assets/images/snowflake-particle.png");
		this.load.image("glow",   	 		"assets/images/cinder.png");
		this.load.image("explosion",   	"assets/images/explosion.png");

		// for input effects
		this.load.image("sideglow",   	"assets/images/side-glow.png");

		// assets for gameOverDisplay
		this.load.image("enter",   			"assets/images/enter.png");

		// assets for throwCountdownDisplay
		this.load.image("badge",   			"assets/images/shield.png");
		this.load.bitmapFont('black',		'assets/fonts/SFFedora.png', 'assets/fonts/SFFedora.xml');

		// fonts for particle effects
		this.load.bitmapFont('vcrosdmn', 'assets/fonts/vcrosdmono.png', 'assets/fonts/vcrosdmono.xml');
		this.load.bitmapFont('sffedora', 'assets/fonts/SFFedoraWhite.png', 'assets/fonts/SFFedoraWhite.xml');
		this.load.bitmapFont('finalold', 'assets/fonts/FINALOLD.png', 'assets/fonts/FINALOLD.xml');
		this.load.bitmapFont('fadetogr', 'assets/fonts/FADETOGR.png', 'assets/fonts/FADETOGR.xml');

		// audio for countdown
		this.load.audio("countdown_click", ["assets/soundfx/countdown_click.mp3"]);
		this.load.audio("countdown_end", ["assets/soundfx/countdown_siren.mp3"]);

		// audio for game piece effects
		this.load.audio("game_activate", ["assets/soundfx/game_activate.mp3"]);
		this.load.audio("game_bounce", ["assets/soundfx/game_bounce.mp3"]);
		this.load.audio("game_burn", ["assets/soundfx/game_burn.mp3"]);
		this.load.audio("game_combo_over", ["assets/soundfx/game_combo_over.mp3"]);
		this.load.audio("game_combo_up", ["assets/soundfx/game_combo_up.mp3"]);
		this.load.audio("game_disappear", ["assets/soundfx/game_disappear.mp3"]);
		this.load.audio("game_drop", ["assets/soundfx/game_drop.mp3"]);
		this.load.audio("game_explode", ["assets/soundfx/game_explode.mp3"]);
		this.load.audio("game_fire_extinguish", ["assets/soundfx/game_fire_extinguish.mp3"]);
		this.load.audio("game_freeze", ["assets/soundfx/game_freeze.mp3"]);
		this.load.audio("game_set", ["assets/soundfx/game_set.mp3"]);
		this.load.audio("game_stone_appear", ["assets/soundfx/game_stone_appear.mp3"]);
		this.load.audio("game_stone_disappear", ["assets/soundfx/game_stone_disappear.mp3"]);
		this.load.audio("game_toast", ["assets/soundfx/game_toast.mp3"]);
		this.load.audio("game_victory_cheer", ["assets/soundfx/game_victory_cheer.mp3"]);

		// music
		this.load.audio(this.key + "_music", [this.musicAsset]);
		this.load.audio("gameover", "assets/music/Victorious-Short.mp3" );

		// load the background
		if( this.bg !== null )
		{
			this.load.image( this.key + "_game_bg", this.bg );
		}
  }

	// Handle piece transitions.
	freeze( x, y )
	{
		this.particleManager.freezeParticles( x, y );
		this.soundEffects.freeze.play()
	}
	unfreeze( x, y )
	{
		this.particleManager.steamParticles( x, y );
		this.soundEffects.fire_extinguish.play();
	}
	evaporate( x, y )
	{
		this.particleManager.steamParticles( x, y );
		this.soundEffects.fire_extinguish.play();
	}
	burn( x, y )
	{
		this.particleManager.smokeParticles( x, y );
		this.soundEffects.burn.play();
	}
	toast( x, y )
	{
		this.particleManager.smokeParticles( x, y );
		this.soundEffects.toast.play();
	}
	activate( x, y )
	{
		this.particleManager.activateParticles( x, y );
		this.soundEffects.activate.play();
	}
	explode( x, y )
	{
		this.particleManager.starParticles( x, y );
		this.particleManager.explosionParticles( x, y );
		this.soundEffects.explode.play();
	}
	star( x, y )
	{
		this.particleManager.starParticles( x, y );
		this.soundEffects.disappear.play();
	}
	score( x, y, text, size )
	{
		this.particleManager.showScore( x, y, text, size );
	}

	create()
	{
		super.create();

		// if we have set a bg, load it
		if( this.bg !== null )
		{
			let bgImage = this.add.image(
				this.cameras.main.centerX,
				this.cameras.main.centerY,
				this.key + "_game_bg"
			);

			// scale the bg image
			bgImage.scale = this.sys.game.canvas.height / bgImage.height;
		}

		// add our side glows
		this.glows.down = this.make.image({
	    x: this.cameras.main.centerX,
	    y: this.sys.game.canvas.height - 20,
	    key: "sideglow",
			alpha: 0,
			add: true
		});
		this.glows.left = this.make.image({
	    x: 20,
	    y: this.cameras.main.centerY,
	    key: "sideglow",
	    angle: 90,
			alpha: 0,
			add: true
		});
		this.glows.up = this.make.image({
	    x: this.cameras.main.centerX,
	    y: 20,
	    key: "sideglow",
	    angle: 180,
			alpha: 0,
			add: true
		});
		this.glows.right = this.make.image({
	    x: this.sys.game.canvas.width - 20,
	    y: this.cameras.main.centerY,
	    key: "sideglow",
	    angle: 270,
			alpha: 0,
			add: true
		});

		this.particleManager = new ParticleManager(this);

		this.soundEffects = {
			countdown_click: this.sound.add("countdown_click"),
			countdown_end: this.sound.add("countdown_end"),
			activate: this.sound.add("game_activate"),
			bounce: this.sound.add("game_bounce"),
			burn: this.sound.add("game_burn"),
			combo_over: this.sound.add("game_combo_over"),
			combo_up: this.sound.add("game_combo_up"),
			disappear: this.sound.add("game_disappear"),
			drop: this.sound.add("game_drop"),
			explode: this.sound.add("game_explode"),
			fire_extinguish: this.sound.add("game_fire_extinguish"),
			freeze: this.sound.add("game_freeze"),
			set: this.sound.add("game_set"),
			stone_appear: this.sound.add("game_stone_appear"),
			stone_disappear: this.sound.add("game_stone_disappear"),
			toast: this.sound.add("game_toast"),
			victory_cheer: this.sound.add("game_victory_cheer"),
		};

		// set all the sound effects to the appropriate levels
		for (let p in this.soundEffects) {
			this.soundEffects[p].setVolume(Options.soundfxVolume);
		}

		this.music = this.sound.add(this.key + "_music");
		this.music.setVolume(Options.musicVolume);
		this.music.setLoop(true);
		this.music.play();

		this.gameOverMusic = this.sound.add("gameover");
		this.gameOverMusic.setLoop(true);
		this.gameOverMusic.setVolume(Options.musicVolume);


		// create the object to translate user input
		this.defineInputHandler();

		// create the game grid
		this.defineGameGrid();
		if( this.gameGrid !== null )
		{
			// for handling sound effects
			//this.gameGrid.on( GameGrid.PIECE_FALLING, () => this.soundEffects.drop.play() );
			//this.gameGrid.on( GameGrid.GAME_BOUNCE, () => this.soundEffects.bounce.play() );
			this.gameGrid.on( GameGrid.GAME_SET, () => this.soundEffects.set.play() );

			// handle particle effects
			this.gameGrid.on( GameGrid.PIECE_CLEAR, (data) => this.star(data.x, data.y)  );
			this.gameGrid.on( GameGrid.PIECE_FREEZE, (data) => this.freeze(data.x, data.y)  );
			this.gameGrid.on( GameGrid.PIECE_EXTINGUISH, (data) => this.evaporate(data.x, data.y)  );
			this.gameGrid.on( GameGrid.PIECE_THAW, (data) => this.unfreeze(data.x, data.y)  );
			this.gameGrid.on( GameGrid.PIECE_BURN, (data) => this.burn(data.x, data.y)  );
			this.gameGrid.on( GameGrid.PIECE_TOAST, (data) => this.toast(data.x, data.y)  );
			this.gameGrid.on( GameGrid.PIECE_ACTIVATE, (data) => this.activate(data.x, data.y)  );
			this.gameGrid.on( GameGrid.PIECE_EXPLODE, (data) => this.explode(data.x, data.y)  );
		}

		// create the display element for the throw countdown
		this.defineThrowCountdownDisplay();

		// create the display element that shows the next piece
		this.defineNextPieceDisplay();

		// create the display element that shows our game status
		this.defineInfoDisplay();

    // create a ComboCounter to listen to
    this.defineComboCounter();

		// create a StartCountdown
		this.defineStartCountdown();

		// create a GameOverDisplay
		this.defineGameOverDisplay();
	}


	defineNextPieceGenerator()
	{
		this.nextPieceGenerator = new NextPieceGenerator();
	}

	defineThrowManager()
	{
		this.throwManager = new ThrowManager(this.nextPieceGenerator);
	}

	defineComboCounter()
	{
		this.comboCounter = new ComboCounter({
      scene:this,
      displayY: this.cameras.main.centerY - 120,
    });
    this.comboCounter.on( ComboCounter.COMBO_UP, () => this.soundEffects.combo_up.play() );
    this.comboCounter.on( ComboCounter.COMBO_OVER, () => this.soundEffects.combo_over.play() );
	}

	defineGameGrid() {}
	defineNextPieceDisplay() {}
	defineThrowCountdownDisplay() {}
	defineInfoDisplay() {}

	defineClearHandler()
  {
    this.clearHandler = new ClearHandler();
  }

	defineInputHandler()
	{
		this.inputHandler = new InputHandler();
    this.inputHandler.on( InputHandler.DOWN, () => {
			this.gameGrid.dropFallingPiece();
			this.glow("down");
		} );
    this.inputHandler.on( InputHandler.LEFT, () => {
			this.gameGrid.moveFallingPieceLeft();
			this.glow("left");
		} );
    this.inputHandler.on( InputHandler.RIGHT, () => {
			this.gameGrid.moveFallingPieceRight();
			this.glow("right");
		} );
    this.inputHandler.on( InputHandler.UP, () => {
		  this.gameGrid.rotateFallingPiece();
	  	this.glow("up");
		} );
	}

	defineStartCountdown()
	{
		this.startCountdown = new StartCountdown({scene:this, displayY:this.cameras.main.centerY/2});
		this.startCountdown.on( StartCountdown.COUNTDOWN_CLICK, () => this.soundEffects.countdown_click.play() );
		this.startCountdown.on( StartCountdown.COUNTDOWN_END, () => {
			this.soundEffects.countdown_end.setVolume(0.1);
			this.soundEffects.countdown_end.play();
			this.startCountdown.destroy();

			// start the game
			this.start();
		} );
		//this.startCountdown.start();
	}

	defineGameOverDisplay()
	{
		this.gameOverDisplay = new GameOverDisplay({scene:this, displayY:this.cameras.main.centerY/2});
	}

	// start the game
	start()
	{
		if( this.gameGrid === null ) console.error("GameScene start() cannot start without gameGrid defined");

		this.gameGrid.on(
			GameGrid.GAME_NEWROUND,
			(data) => this.time.delayedCall(
				GameGrid.TIME_BUFFER,
				() => this.logic(data)
			)
		);

		if( this.handleThrow( this.gameGrid.gamePieces ) )
		{
			return;
		}
		else
		{
			this.loadNextPieces();
			this.gameGrid.addPiece();
		}
	}


	// end the game
	stop()
	{
		this.comboCounter.destroy();
		this.inputHandler.destroy();
		if( this.music !== null ) this.music.stop();

		// bring the infoDisplay to the top
		this.children.bringToTop( this.infoDisplay );

		// run the tweens
		let timeline = this.tweens.createTimeline();

		timeline.add({
			targets: [this.gameGrid, this.nextPieceDisplay, this.throwCountdownDisplay],
			alpha: 0,
			duration: 3000,
			ease: 'Quad.easeInOut',
		});
		timeline.add({
			targets: this.infoDisplay,
			x: this.cameras.main.centerX,
			y: this.cameras.main.centerY,
			scale: 1.5,
			duration: 1000,
			ease: 'Quad.easeInOut',
		});
		timeline.play();

		this.gameOverMusic.play();

		if( this.gameOverDisplay !== null )
		{
			this.gameOverDisplay.show();
		}

		this.inputHandler = new InputHandler();
		this.inputHandler.on( InputHandler.ENTER, () => this.gameOverConfirmed() );

	}

	/*
	 * When game over is confirmed, take action.
	 */
	gameOverConfirmed()
	{
		this.inputHandler.off( InputHandler.ENTER, () => this.gameOverConfirmed() );
		this.tweens.add({
			targets:this.gameOverMusic,
			volume:0,
			duration:300,
			onComplete: () => this.gameOverMusic.stop()
		});
	}

	keyPress(value)
  {
		//super.keyPress(value);
		if( this.inputHandler !== null )
		{
			this.inputHandler.handle(value);
		}
	}


	loadNextPieces()
	{
		let piecesToGenerate = null;

		if( this.nextPieceDisplay !== null )
		{
			// get the pieces we need from our nextPieceDisplay
			piecesToGenerate = this.nextPieceDisplay.loadNewPieces(
				this.getNextPieceType(),
				this.getNextPieceType()
			);
		}

		if( piecesToGenerate === null ) console.error("GameScene.loadNextPieces()");

		this.gameGrid.nextPieceArray = [
			this.generateGamePiece(piecesToGenerate[0]),
			this.generateGamePiece(piecesToGenerate[1])
		];

	}


	getNextPieceType()
	{
		if( this.nextPieceGenerator === null ) {
			console.error("GameScene getNextPiece() - no nextPieceGenerator exists");
		}

		return this.nextPieceGenerator.generatePieceType();
	}


	generateGamePiece( type )
	{
		switch( type )
		{
			case NextPieceGenerator.BRICK:
				return new Brick({scene:this});
				break;
			case NextPieceGenerator.WOOD:
				return new Wood({scene:this});
				break;
			case NextPieceGenerator.FIRE:
				return new Wood({scene:this, burning:true});
				break;
			case NextPieceGenerator.WATER:
				return new Water({scene:this});
				break;
			case NextPieceGenerator.ICE:
				return new Water({scene:this, frozen:true});
				break;
			case NextPieceGenerator.MARSHMALLOW:
				return new Marshmallow({scene:this});
				break;
			case NextPieceGenerator.BOMB:
				return new Bomb({scene:this});
				break;
		}
	}

	generateThrowPieces( throwArr )
	{
		let result = [];
		for( let i = 0; i < throwArr.length; i++ )
		{
			result.push([]);
			for( let j=0; j < throwArr[i].length; j++ )
			{
				if( throwArr[i][j] !== 0 ) {
					result[i][j] = this.generateGamePiece(throwArr[i][j]);
				} else result[i][j] = 0;
			}
		}
		return result;
	}


	/*
	 * Pass the state of the gameGrid to a new GameLogic to determine what reactions should happen.
	 * If there are reactions that should happen, pass them to handleReactions().
	 */
	logic(data)
  {
    let logic = new GameLogic( data.grid );
    let reactions = logic.reactionCheck();

    if( this.gameGrid === null ) return;

    if( reactions.length < 1 )
    {
      // there are no more reactions to handle, game can proceed
      this.time.delayedCall(
        GameGrid.TIME_BUFFER,
        () => {

					// check for any throws
					if( this.handleThrow(data.grid) ) return;

					// check for the game over condition
          if( !this.gameGrid.isDropAreaFree() )
					{
						return this.stop();
					}

					// seed the gameGrid's next piece
          this.loadNextPieces();

					// and add the piece
					this.gameGrid.addPiece();

        }
      );

			// reset our ComboCounter
			if( this.comboCounter !== null ) this.comboCounter.reset();

    }
    else {
      this.time.delayedCall(
        this.handleReactions( reactions ) + GameGrid.TIME_BUFFER,
        () => this.gameGrid.check()
      );
    }
  }


	handleThrow( gameGrid )
	{
		if( this.throwManager !== null )
		{
			let throws = this.throwManager.getThrow( gameGrid );
			// if there is a throw, send it to the gameGrid
			if( throws !== null )
			{
				// after the throw has been added, trigger a reaction check
				this.time.delayedCall(
					this.gameGrid.addThrow( this.generateThrowPieces( throws ) ) + GameGrid.TIME_BUFFER,
					() => this.gameGrid.check()
				);
				return true;
			}
		}

		// update the throw countdown
		if( this.throwCountdownDisplay !== null )
		{
			this.throwCountdownDisplay.show( this.throwManager.movesRemaining );
		}
		if( !this.throwManager.movesRemaining )
		{
			this.handleThrowWarning();
		}

		return false;
	}


	/*
	 * Go through the reactions array, trigger appropriate response in gameGrid.
	 */
	handleReactions( reactions )
	{
		let time = 0;

		// variables for possible score showings
		let avgX = 0;
		let avgY = 0;
		let topY = this.sys.game.canvas.height;

		let countsAsCombo = false;
		let comboText = null;

		for( let i=0; i<reactions.length; i++)
		{

			// set locations for possible score showings
			avgX += this.gameGrid.x + this.gameGrid.getPieceX( reactions[i].i );
			avgY += this.gameGrid.y + this.gameGrid.getPieceY( reactions[i].j );
			topY = Math.min(
				topY,
				this.gameGrid.y + this.gameGrid.getPieceY( reactions[i].j )
			);

			if( reactions[i] instanceof Gravity )
			{
				time = Math.max( time, this.gameGrid.pieceFalling(
					reactions[i].i,
					reactions[i].j,
					reactions[i].data.spaces
				) );

				// if something was thrown, we should play a falling sound effect
				//console.log( this.throwManager.moveSinceLastThrow );
				if( !this.throwManager.moveSinceLastThrow ) this.soundEffects.drop.play();

			} else if( reactions[i] instanceof Clear ) {
				time = Math.max( time, this.gameGrid.pieceClear(
					reactions[i].i,
					reactions[i].j,
					reactions[i].data.delayFactor
				) );

				countsAsCombo = true;
				comboText = "CLEAR X"+reactions.length;

			} else if( reactions[i] instanceof Freeze ) {
				time = Math.max( time, this.gameGrid.pieceFreeze(
					reactions[i].i,
					reactions[i].j,
					reactions[i].data.delayFactor
				) );
			} else if( reactions[i] instanceof Extinguish ) {
				time = Math.max( time, this.gameGrid.pieceExtinguish(
					reactions[i].i,
					reactions[i].j
				) );

				countsAsCombo = true;
				comboText = "EXT X"+(reactions.length-1);

			} else if( reactions[i] instanceof Burn ) {
				time = Math.max( time, this.gameGrid.pieceBurn(
					reactions[i].i,
					reactions[i].j,
					reactions[i].data.delayFactor
				) );
			} else if( reactions[i] instanceof Toast ) {
				time = Math.max( time, this.gameGrid.pieceToast(
					reactions[i].i,
					reactions[i].j
				) );

				countsAsCombo = true;
				comboText = "TOAST!";

			} else if( reactions[i] instanceof Explode ) {
				time = Math.max( time, this.gameGrid.pieceExplode(
					reactions[i].i,
					reactions[i].j,
					reactions[i].level,
					reactions[i].exp
				) );

				countsAsCombo = true;
				comboText = "BOOM!";

			}
		}

		// calculate where we should show score and combo notifications
		avgX = avgX / reactions.length;
		avgY = avgY / reactions.length;
		topY -= GamePiece.gamePieceWidth / 2;

		if( countsAsCombo && this.comboCounter !== null ) this.comboCounter.combo();

		// handle score count
		let score = 0;
		if( this.clearHandler !== null
			&& this.comboCounter !== null ) score = this.clearHandler.handleClears( reactions, this.comboCounter.count );

		if( score && this.infoDisplay !== null )
		{

			this.time.delayedCall(
        time,
        () => {
					// update our infoDisplay with the current score
					this.infoDisplay.display({score:score.total});

					// show our score text
					this.score(avgX, avgY, score.score, 7 + Math.ceil(score.score / 50));
				}
      );

			this.time.delayedCall(
        time + 400,
        () => {
					// show our combo text
					this.score(avgX, topY, comboText, 7 + reactions.length * 3);
				}
      );


		}

		return time;
	}


	/*
	 * Do something dramatic so that player knows a throw is coming!
	 */
	handleThrowWarning() {}


	glow( direction )
	{
		let timeline = this.tweens.createTimeline();
		timeline.add({
			targets: this.glows[direction],
			alpha: 0.5,
			duration: 100,
			ease: 'Quad.easeOut',
		});
		timeline.add({
			targets: this.glows[direction],
			alpha: 0,
			duration: 1000,
			ease: 'Quad.easeOut',
		});
		timeline.play();
	}

	/*
	 * Manually update our GameGrid.
	 */
	update( args ) {
		super.update(args);
		if( this.gameGrid !== null ) this.gameGrid.update();
	}

}
