import Phaser from 'phaser'
import SceneWithInput from '../scenes/SceneWithInput';

import Options from '../control/Options';

export default class TextMenu extends SceneWithInput
{

  /*
  * config object should contain an array of objects {title, function}
  */
  constructor(config)
  {
    super(config);

    /*
    this.menu = [
      {title: "Test 1", function: ()=>console.log("Test 1 clicked.") },
      {title: "Test 2", function: ()=>console.log("Test 2 clicked.") },
      {title: "Go back one.", function: ()=>this.changeButton(-1) },
    ];
    console.log( this.menu );
    */

    if( config === null )
    {
      console.error("TextMenu was created without menu options.");
      return;
    }

    // generate a unique id for the assets of this scene
		this.key = Math.random().toString(36).substring(7) + "_" + config.key + "_";

    // we'll save the menu to be created in create()
    this.menu = config.menu;

    // an array of buttons that can be clicked on
    this.buttons = [];

    // which button is currently selected
    this.currentButton = 0;

    // save the sound config object for preload
    if( 'sound' in config ) this.soundConfig = config.sound;

    // the title to display
    if( 'title' in config ) this._title = config.title;

    // stores the font sizes for animations
    this.fontSizes = {
      selected: 72,
      notSelected: 42
    };
  }

  preload()
  {
    //this.load.bitmapFont('sffedora', 'assets/fonts/SFFedora.png', 'assets/fonts/SFFedora.xml');
    this.load.bitmapFont('fadetogr', 'assets/fonts/FADETOGR.png', 'assets/fonts/FADETOGR.xml');
    this.load.bitmapFont('finalold', 'assets/fonts/FINALOLD.png', 'assets/fonts/FINALOLD.xml');

    // cursor
    this.load.image("cross", "assets/images/fire-cross.png");

    // load any sound from this.sound object
    if( 'music' in this.soundConfig ) {
      this.load.audio(this.key + "menu_music", this.soundConfig.music );
      this.musicPresent = true;
    }
    if( 'switch' in this.soundConfig ) {
      this.load.audio("switch", this.soundConfig.switch );
      this.switchPresent = true;
    }
    if( 'select' in this.soundConfig ) {
      this.load.audio("select", this.soundConfig.select );
      this.selectPresent = true;
    }
  }

  create()
  {
    super.create();

    let title_headspace = 0.15;
    let title_size = 60;

    if( this._title !== null ) {
      let t = this.add.bitmapText(
        this.cameras.main.centerX,
        this.sys.game.canvas.height * title_headspace,
        'fadetogr',
        this._title,
        title_size
      );
      t.setCenterAlign();
      t.setOrigin(0.5, 0);
    }

    let dist_between_buttons = 30;
    let text_size = this.fontSizes.notSelected;
    let currentY = this.cameras.main.centerY - ((this.menu.length / 2) * (text_size+dist_between_buttons));
    if( this._title !== null ) {
      currentY += (this.sys.game.canvas.height * title_headspace)*0.8;
    }

    for( let i=0; i<this.menu.length; i++ )
    {
      let b = this.add.bitmapText(
        this.cameras.main.centerX,
        currentY,
        'finalold',
        this.menu[i].title,
        text_size
      );
      if( i === this.currentButton ) b.fontSize = this.fontSizes.selected;
      b.setOrigin(0.5);
      //b.setCenterAlign();

      if(this.listenToClicks)
      {
        b.setInteractive();
        b.on('pointerover', () => this.mouseOver(i));
        b.on('clicked', () => this.activateCurrentButton());
      }

      this.buttons.push( b );
      currentY += dist_between_buttons + text_size;
    }

    // add our cross cursors
    this.cross_dist_to_text = 60;
    this.cross_dim = 30;
    let p = this.getCrossPositions(0, this.cross_dist_to_text, this.cross_dim);

    this.crosses = [
      this.add.sprite(
        p[0].x,
        p[0].y,
        'cross'
      ),
      this.add.sprite(
        p[1].x,
        p[1].y,
        'cross'
      ),
    ];

    for(let i=0; i<this.crosses.length; i++)
    {
      this.crosses[i].scale = this.cross_dim / this.crosses[i].width;
      this.crosses[i].alpha = 0.5;
    }

    // load sounds
    if( this.musicPresent ) {
      this.menu_music = this.sound.add(this.key + "menu_music");
      this.menu_music.setLoop(true);
      this.menu_music.setVolume(Options.musicVolume);

      // play the music
      this.menu_music.play();
    }
    if( this.switchPresent ) {
      this._switch = this.sound.add("switch");
      this._switch.setVolume(Options.soundfxVolume);
    }
    if( this.selectPresent ) {
      this._select = this.sound.add("select");
      this._select.setVolume(Options.soundfxVolume);
    }
  }

  // return an array of objects with x,y positions for crosses for text at index
  getCrossPositions(index, cross_dist_to_text, cross_dim)
  {
    let b = this.buttons[index];
    return [
      {
        x: b.x - (b.width*0.5) - cross_dist_to_text - (cross_dim*0.5),
        y: b.y - 4
      },
      {
        x: b.x + (b.width*0.5) + cross_dist_to_text + (cross_dim*0.5),
        y: b.y - 4
      }
    ];
  }

  setActiveButton(num)
  {
    // shrink any active buttons
    if( this.currentButton >= 0 )
    {
      this.tweens.add({
        targets: this.buttons[ this.currentButton ],
        //scale: 1,
        fontSize: this.fontSizes.notSelected,
        duration: 200,
        ease: 'Quad.easeInOut',
      });
    }

    // mark the new currentButton
    this.currentButton = num;

    // play a switch sound if present
    if( this.switchPresent )
    {
      this._switch.play();
    }

    // move cross cursors
    let p = this.getCrossPositions(num, this.cross_dist_to_text, this.cross_dim);
    this.tweens.add({
      targets: this.crosses[0],
      duration: 200,
      ease: 'Quad.easeInOut',
      x: p[0].x,
      y: p[0].y
    });
    this.tweens.add({
      targets: this.crosses[1],
      duration: 200,
      ease: 'Quad.easeInOut',
      x: p[1].x,
      y: p[1].y
    });

    // enlarge new current button
    this.tweens.add({
      targets: this.buttons[ this.currentButton ],
      //scale: 1.5,
      fontSize: this.fontSizes.selected,
      duration: 200,
      ease: 'Quad.easeInOut',
    });
  }

  keyPress(value)
  {
    super.keyPress(value);
    if( value === 'ArrowDown' )
    {
      this.changeButton(1);
    }
    else if( value === 'ArrowUp' )
    {
      this.changeButton(-1);
    }
    else if( value === 'Enter' )
    {
      this.activateCurrentButton();
    }
  }

  changeButton(value)
  {
    if( this.currentButton + value >= this.buttons.length )
    {
      return this.setActiveButton(0);
    }
    else if( this.currentButton + value < 0 )
    {
      return this.setActiveButton( this.buttons.length - 1 );
    }
    else return this.setActiveButton( this.currentButton + value );
  }

  activateCurrentButton()
  {
    // play associated sound
    if( this.selectPresent )
    {
      this._select.play();
    }

    this.menu[ this.currentButton ].function.call();
  }

  mouseOver(index)
  {
    if( index === this.currentButton ) return;
    super.mouseOver(index);
    this.setActiveButton(index);
  }

  /*
  close()
  {
    super.close();

    for( let i = 0; i < this.buttons.length; i++ )
    {
      this.buttons[i].destroy();
    }
    this.crosses[0].destroy();
    this.crosses[1].destroy();
  }
  */

}
