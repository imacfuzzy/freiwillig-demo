/*
 * Handles what happens when scoring events occur.
 * Should be extended for single player, multiplayer.
 */
export default class ClearHandler extends Object
{
  constructor( config = null )
  {
    super();
    this.multipliers = {};

    this.multipliers.clear = (config !== null && 'clear' in config.multipliers) ? config.multipliers.clear : 1;
    this.multipliers.extinguish = (config !== null && 'extinguish' in config.multipliers) ? config.multipliers.extinguish : 0.25;
    this.multipliers.toast = (config !== null && 'toast' in config.multipliers) ? config.multipliers.toast : 0.5;
    this.multipliers.explode = (config !== null && 'explode' in config.multipliers) ? config.multipliers.explode : 0.2;

    // for arcade mode, we'll keep track of a score
    this.score = 0;
  }

  reset()
  {
    this.score = 0;
  }

  determineRoundMultiplier( round )
  {
    return round * round;
  }

  handleClears( reactions, round )
  {
    if( !reactions.length ) return;
    if( !(reactions[0].type in this.multipliers) ) return;

    let multiplier = this.multipliers[ reactions[0].type ];
    let roundMultiplier = this.determineRoundMultiplier( round );
    let roundBonus = Math.pow( reactions.length - 5, 2 );
    let add = Math.round( 10 * (reactions.length + roundBonus) * multiplier * roundMultiplier );
    this.score += add;

    //console.log( "score: " + this.score );

    return {total: this.score, score: add};
  }
}
