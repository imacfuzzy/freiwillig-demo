import Reaction from '../reactions/Reaction';

export default class Freeze extends Reaction
{
  constructor( i, j, delayFactor )
  {
    super( "freeze", i, j, { delayFactor: delayFactor } );
  }

  get delayFactor()
  {
    return this.data.delayFactor;
  }
}
