import Reaction from '../reactions/Reaction';

export default class Clear extends Reaction
{
  constructor( i, j, delayFactor )
  {
    super( "clear", i, j, { delayFactor: delayFactor } );
  }

  get delayFactor()
  {
    return this.data.delayFactor;
  }
}
