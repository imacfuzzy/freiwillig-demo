import Reaction from '../reactions/Reaction';

export default class Gravity extends Reaction
{
  constructor( i, j, spaces )
  {
    super( "gravity", i, j, { spaces: spaces } );
  }

  get spaces()
  {
    return this.data.spaces;
  }
}
