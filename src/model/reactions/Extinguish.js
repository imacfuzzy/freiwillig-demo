import Reaction from '../reactions/Reaction';

export default class Extinguish extends Reaction
{
  constructor( i, j, type )
  {
    super( "extinguish", i, j );
    this.ext = type;
  }
}
