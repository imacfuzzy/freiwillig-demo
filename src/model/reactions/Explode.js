import Reaction from '../reactions/Reaction';

export default class Explode extends Reaction
{
  static get EXPLODE_BOMB() { return "EXPLODE_BOMB"; }
  static get EXPLODE_OTHER() { return "EXPLODE_OTHER"; }

  constructor( i, j, level, type )
  {
    super( "explode", i, j, { level: level, exp: type } );
  }

  get level()
  {
    return this.data.level;
  }

  get exp()
  {
    return this.data.exp;
  }
}
