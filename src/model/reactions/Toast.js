import Reaction from '../reactions/Reaction';

export default class Toast extends Reaction
{
  constructor( i, j, delayFactor )
  {
    super( "toast", i, j );
  }
}
