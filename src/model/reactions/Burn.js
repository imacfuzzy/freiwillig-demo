import Reaction from '../reactions/Reaction';

export default class Burn extends Reaction
{
  constructor( i, j, delayFactor )
  {
    super( "burn", i, j, { delayFactor: delayFactor } );
  }

  get delayFactor()
  {
    return this.data.delayFactor;
  }
}
