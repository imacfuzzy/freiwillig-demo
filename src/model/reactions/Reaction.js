export default class Reaction extends Object
{
  constructor( type, i, j, data = {} )
  {
    super();
    this.type = type;
    this.i = i;
    this.j = j;
    this.data = data;
  }

  /*
  static get types()
  {
    return {
      gravity: "gravity",
      bomb: "bomb",
      toast: "toast",
      fire: "fire",
      extinguish: "extinguish",
      ice: "ice",
      clear: "clear"
    };
  }
  */
}
