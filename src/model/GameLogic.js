import Wood from '../graphics/Wood';
import Water from '../graphics/Water';
import Bomb from '../graphics/Bomb';
import Marshmallow from '../graphics/Marshmallow';
import GamePiece from '../graphics/GamePiece';

import Gravity from '../model/reactions/Gravity';
import Clear from '../model/reactions/Clear';
import Freeze from '../model/reactions/Freeze';
import Extinguish from '../model/reactions/Extinguish';
import Burn from '../model/reactions/Burn';
import Toast from '../model/reactions/Toast';
import Explode from '../model/reactions/Explode';

/*
 * Handles the computation of grid status and reactions.
 */
export default class GameLogic
{
  static get CLEAR_LENGTH() { return 5; }

  /*
   * Should receive a nested array representing current game grid.
   */
  constructor( gameGrid )
  {
    this.gameGrid = gameGrid;

    this.events = {
      GAMEGRID_SCORE: "GAMEGRID_SCORE",
      GAMEGRID_COMBO: "GAMEGRID_COMBO",
      PIECE_DISAPPEAR: "PIECE_DISAPPEAR",
    }

    this.clears = {
      SCORE_CLEAR: "clear",
    }
  }


  arrayContainsCoords( arr, x, y )
	{
		for ( let i = 0; i < arr.length; i++ )
		{
			if ( arr[i].x === x && arr[i].y === y ) return true;
		}
		return false;
	}


  getGamePieceType( arg )
  {
    if( arg instanceof GamePiece )
    {
      return arg.type;
    }
    return 0;
  }


  /*
   * Returns an array of all GamePieces in gameGrid of type "type" adjacent to position x,y.
   */
  checkForChain( x, y, type, r = null ) {

		if ( r === null ) r = [];
		r.push( {
			x:x,
			y:y,
			//type: this.getGamePieceType( this.gameGrid[x][y] ),
			frozen:( (this.gameGrid[x][y] instanceof Water ) && this.gameGrid[x][y].frozen ),
			burning:( (this.gameGrid[x][y] instanceof Wood ) && this.gameGrid[x][y].burning )
		} );


		if ( x < this.gameGrid.length - 1
      && this.gameGrid[x + 1][y] instanceof GamePiece
      && this.gameGrid[x + 1][y].type === type
      && !(this.arrayContainsCoords(r,x+1,y))
    ) {
		    r = this.checkForChain(x+1, y, type, r);
  	}
		if ( y < this.gameGrid[0].length - 1
      && this.gameGrid[x][y + 1] instanceof GamePiece
      && this.gameGrid[x][y + 1].type === type
      && !(this.arrayContainsCoords(r,x,y+1))
    ) {
			r = this.checkForChain(x, y+1, type, r);
		}
		if ( x > 0
      && this.gameGrid[x - 1][y] instanceof GamePiece
      && this.gameGrid[x - 1][y].type === type
      && !(this.arrayContainsCoords(r,x-1,y))
    ) {
			r = this.checkForChain(x-1, y, type, r);
		}
		if ( y > 0
      && this.gameGrid[x][y - 1] instanceof GamePiece
      && this.gameGrid[x][y - 1].type === type
      && !(this.arrayContainsCoords(r,x,y-1))
    ) {
			r = this.checkForChain(x, y-1, type, r);
		}

		return r;
	}


  /*
   * Return { x, y } of any pieces directly above, below, left or right of x,y.
   */
  getAdjacentGamePieces( x, y ) {
		let result = [];
		if ( y < this.gameGrid[x].length - 1 && this.gameGrid[x][y + 1] ) result.push( {x:x, y:y+1} );
		if ( y > 0 && this.gameGrid[x][y - 1] ) result.push( {x:x, y:y-1} );
		if ( x > 0 && this.gameGrid[x - 1][y] ) result.push( {x:x-1, y:y } );
		if ( x < this.gameGrid.length - 1 && this.gameGrid[x + 1][y] ) result.push( {x:x+1, y:y} );
		return result;
	}


  /*
   * Return { x, y } of any pieces within 1 space laterally, horizontally, or diagonally.
   */
  getSurroundingGamePieces( x, y ) {
			var result = [];
			for ( let i = (x - 1); i <= (x + 1); i++ ) {
				for ( let j = (y - 1); j <= (y + 1); j++ ) {
					if ( i < 0 || i >= this.gameGrid.length || j < 0 || j >= this.gameGrid[i].length ) continue;
          if ( i === x && j === y ) continue;
					if ( this.gameGrid[i][j] instanceof GamePiece ) result.push( {x:i,y:j} );
				}
			}
			return result;
		}


  /*
   * Return an array of { x, y } for GamePieces of type "type" at positions { x,y } in arr.
   */
  filterGamePieceArray( arr, type ) {
		let result = [];
		for ( let i = 0; i < arr.length; i++ )
		{
			let o = this.gameGrid[arr[i].x][arr[i].y];
      if( !o ) continue;
			if ( o.type === type ) result.push( arr[i] );
		}
		return result;
  }

  /*
  * 1. Make sure all pieces have fallen as far as they can fall.
  */
  _gravity() {
		let settled = [];
		for ( let i = 0; i < this.gameGrid.length; i++ ) {
			let spaces = 0;
			for ( let j = this.gameGrid[i].length - 1; j >= 0; j-- ) {
				if ( !this.gameGrid[i][j] )
				{
					spaces++;
				}
				else if ( spaces > 0 )
				{
          settled.push( new Gravity( i, j, spaces ) );
				}
			}
		}
		return settled;
	}


  /*
   * 2. Explode any bombs next to fires.
   */
  _bomb() {
    let bombsToExplode = [];
    let contained = [];
    let bombsToProcess = [];

		// find all bombs directly next to flames
		for ( let i = 0; i < this.gameGrid.length; i++ ) {
			for ( let j = 0; j < this.gameGrid[i].length; j++ ) {
				if ( this.gameGrid[i][j] instanceof Wood && this.gameGrid[i][j].burning ) {

          // check to see if any fires are adjacent
					let bombs = this.filterGamePieceArray(
            this.getAdjacentGamePieces( i, j ),
            "bomb"
          );
					if ( !bombs.length ) continue;

          while( bombs.length )
          {
            let bombNextToFire = bombs.shift();

            // make sure we haven't already marked this bomb for exploding
            if( this.arrayContainsCoords( contained, bombNextToFire.x, bombNextToFire.y ) ) continue;

            // put our bomb in the queue for processing
            bombsToProcess.push( { x:bombNextToFire.x, y:bombNextToFire.y, level:0 } );
          }

          while( bombsToProcess.length )
          {
            // shorten our queue
            let b = bombsToProcess.shift();

            if( this.arrayContainsCoords( contained, b.x, b.y ) ) continue;

            contained.push( { x:b.x, y:b.y } );
            bombsToExplode.push( new Explode( b.x, b.y, b.level, Explode.EXPLODE_BOMB ) );

            // also, we'll need to mark the pieces surrounding the bombs for destruction
            let surroundingPieces = this.getSurroundingGamePieces( b.x, b.y );
            for(let s = 0; s < surroundingPieces.length; s++ )
            {
              // make sure we haven't already marked this piece for destruction
              if( this.arrayContainsCoords( contained, surroundingPieces[s].x, surroundingPieces[s].y ) ) continue;

              // if it's a bomb, we need to mark it for its own explosion
              if( this.gameGrid[ surroundingPieces[s].x ][ surroundingPieces[s].y ] instanceof Bomb )
              {
                if( !this.arrayContainsCoords(
                  bombsToProcess,
                  surroundingPieces[s].x,
                  surroundingPieces[s].y
                ) ) bombsToProcess.push({ x:surroundingPieces[s].x, y:surroundingPieces[s].y, level:b.level+1 });
              }
              else // otherwise explode it at the current bombsLevel
              {
                contained.push( { x:surroundingPieces[s].x, y:surroundingPieces[s].y } );
                bombsToExplode.push( new Explode( surroundingPieces[s].x, surroundingPieces[s].y, b.level, Explode.EXPLODE_OTHER ) );
              }
            }
          }
				}
			}
		}
    return bombsToExplode;
	}


  /*
   * 3. Check for any fire next to marshmallows.
   */
   _toast()
   {
     let result = [];
     let contained = [];

     for ( let i = 0; i < this.gameGrid.length; i++ ) {
			for ( let j = 0; j < this.gameGrid[i].length; j++ ) {

        // check all fires to see if marshmallows are adjacent
				if ( this.gameGrid[i][j] instanceof Wood && this.gameGrid[i][j].burning  ) {

          // get all adjacent marshmallows
					let coords = this.filterGamePieceArray(
            this.getAdjacentGamePieces( i, j ),
            "marshmallow"
          );
					if ( !coords.length ) continue;

					for ( let k = 0; k < coords.length; k++ ) {

            // make sure we don't have any already toasted or repeats
            if( this.gameGrid[ coords[k].x ][ coords[k].y ].toasted
              || this.arrayContainsCoords( contained, coords[k].x, coords[k].y ) ) continue;
            contained.push( { x:coords[k].x, y:coords[k].y } );

            result.push( new Toast( coords[k].x, coords[k].y ) );
					}
				}
			}
		}
    return result;
  }


  /*
   * 4. Check for any combustible elements next to fire.
   */
   _fire()
   {
     let burn = [];
     let contained = [];
     for ( let i = 0; i < this.gameGrid.length; i++ ) {
				for ( let j = 0; j < this.gameGrid[i].length; j++ ) {
					if ( this.gameGrid[i][j] instanceof Wood && !this.gameGrid[i][j].burning  ) {

            // see if any fires are adjacent to this wood
						let fires = this.filterGamePieceArray(
              this.getAdjacentGamePieces( i, j ), "fire"
            );
						if ( !fires.length ) continue;

            // ... and if so, get whatever woods are connected to this one
						let chain = this.checkForChain( i, j, "wood" );

						for ( let k = 0; k < chain.length; k++ ) {

              if( chain[k].burning ) continue;

              // make sure we don't have any repeats
              if( this.arrayContainsCoords( contained, chain[k].x, chain[k].y ) ) continue;
              contained.push( { x:chain[k].x, y:chain[k].y } );

              burn.push( new Burn( chain[k].x, chain[k].y, k ) );
						}
					}
				}
			}
      return burn;
   }


  /*
  * 5. Check for any water next to fire.
  */
  _extinguish()
  {
    // fires next to ice or water should disappear
    // water next to fire should be evauporated
    // ice next to fire should be thawed into water

    let result = [];
    let contained = [];

		for ( let i = 0; i < this.gameGrid.length; i++ ) {
			for ( let j = 0; j < this.gameGrid[i].length; j++ ) {

				if ( this.gameGrid[i][j] instanceof Wood && this.gameGrid[i][j].burning ) {

          // make an array with all adjacent water/ice
					let liquids = this.filterGamePieceArray(
            this.getAdjacentGamePieces( i, j ),
            "water"
          );
          let ices = this.filterGamePieceArray(
            this.getAdjacentGamePieces( i, j ),
            "ice"
          );
          liquids.push(...ices);

          // if there is a water/ice present, extinguish the fire
          if( liquids.length > 0
            && !this.arrayContainsCoords( contained, i, j ) ) {
              contained.push( { x:i, y:j } );
              result.push( new Extinguish( i, j, "fire" ));
            }

          // ... and extinguish any water/ice adjacent to the fire
					for ( let k = 0; k < liquids.length; k++ ) {
            // make sure we don't have any repeats
            if( this.arrayContainsCoords( contained, liquids[k].x, liquids[k].y ) ) continue;
            contained.push( { x:liquids[k].x, y:liquids[k].y } );

            result.push( new Extinguish(
              liquids[k].x,
              liquids[k].y,
              this.gameGrid[liquids[k].x][liquids[k].y].type )
            );
					}
				}

			}
		}

    return result;
  }


  /*
  * 6. Check for any freezable elements next to ice.
  */
  _ice() {
    let freeze = [];
    let contained = [];
		for ( let i = 0; i < this.gameGrid.length; i++ ) {
			for ( let j = 0; j < this.gameGrid[i].length; j++ ) {

				if ( this.gameGrid[i][j] instanceof Water && this.gameGrid[i][j].frozen  ) {

          // check to see if any unfrozen waters are adjacent
					let waters = this.filterGamePieceArray(
            this.getAdjacentGamePieces( i, j ),
            "water"
          );
					if ( !waters.length ) continue;

					let chain = this.checkForChain( i, j, "water" );
          chain.shift();

					for ( let k = 0; k < chain.length; k++ ) {

            // don't push anything on that's already frozen
            if( chain[k].frozen ) continue;

            // make sure we don't have any repeats
            if( this.arrayContainsCoords( contained, chain[k].x, chain[k].y ) ) continue;
            contained.push( { x:chain[k].x, y:chain[k].y } );

            freeze.push( new Freeze( chain[k].x, chain[k].y, k ) );
					}
				}
			}
		}
		return freeze;
  }


  /*
  * 7. Check for any chains that we can disappear.
  */
  _clears() {
		let chains = [];
    let contained = [];
		for ( let i = 0; i < this.gameGrid.length; i++ ) {
			for ( let j = 0; j < this.gameGrid[i].length; j++ ) {
				if ( this.gameGrid[i][j] instanceof GamePiece && this.gameGrid[i][j].clears ) {
					let coords = this.checkForChain( i, j, this.gameGrid[i][j].type );
					if ( coords.length >= GameLogic.CLEAR_LENGTH )
					{
						for ( let k = 0; k < coords.length; k++ ) {
              if( this.arrayContainsCoords(contained, coords[k].x, coords[k].y ) ) continue;
              contained.push( { x:coords[k].x, y:coords[k].y } );
              chains.push( new Clear( coords[k].x, coords[k].y, k ) );
						}
					}
				}
			}
		}
		return chains;
	}


  /*
   * Checks the grid, returns actions that need to be taken.
   */
  reactionCheck()
	{

		// 1. Make sure all pieces have fallen as far as they can fall.
		let g = this._gravity();
		if ( g.length > 0 ) {
      return g;
		}

		// 2. Explode any bombs next to fires.
		var b = this._bomb();
		if ( b.length > 0 ) {
			return b;
		}

		// 3. Check for any fire next to marshmallows.
		var t = this._toast();
		if ( t.length > 0 ) {
			return t;
		}

		// 4. Check for any combustible elements next to fire.
		var f = this._fire();
		if ( f.length > 0 ) {
			return f;
		}

		// 5. Check for any water next to fire.
		let e = this._extinguish();
		if ( e.length > 0 ) {
			return e;
		}

		// 6. Check for any freezable elements next to ice.
		let s = this._ice();
		if ( s.length > 0 ) {
			return s;
		}

		// 7. Check for any chains that we can disappear.
    let c = this._clears();
    if ( c.length > 0 ) {
      return c;
    }
    /*
		var c:Number = clears();
		if ( c > 0 ) {
			setTimeout( reactionCheck, (c+BETWEEN_REACTIONS) * 1000 );
			return;
		}


		// END. No more reactions or falling to be done.
		this.dispatchEvent( new Event(GAMEGRID_DONE) );
    */

    return [];

	}
}
