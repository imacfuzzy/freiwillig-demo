import Wood from '../graphics/Wood';
import Water from '../graphics/Water';
import Bomb from '../graphics/Bomb';
import Marshmallow from '../graphics/Marshmallow';
import Brick from '../graphics/Brick';

import NextPieceGenerator from '../model/NextPieceGenerator';

/*
 * Handles the generation of throws for a game.
 */
export default class ThrowManager
{
  static get THROW_DIFFICULTY_MAX() { return 5; }

  constructor( nextPieceGenerator )
  {
    // allows us to generate new pieces
    this.nextPieceGenerator = nextPieceGenerator;

    /*
		 * Current number of throws performed.
		 */
		this.moveCounter = -1;

		/*
		 * How many moves are performed before a throw.
		 */
		this.throwInterval = 10;

		/*
		 * Increasing difficulty enables more difficult throws, makes easier throws less likely.
		 */
		this.throwDifficulty = 1;

    /*
     * Tracks a reduction in turns between throws.
     */
    this.throwIntervalDecrease = 0;
  }

  get movesRemaining()
  {
    return this.throwInterval - this.moveCounter;
  }

  get moveSinceLastThrow()
  {
      return Math.max( 0, this.moveCounter );
  }

  reset()
  {
		this.moveCounter = -1;
		this.throwDifficulty = 1;
		this.throwIntervalDecrease = 0;
		this.throwInterval = 10;
	}

  upDifficulty()
  {
		if( this.throwDifficulty < ThrowManager.THROW_DIFFICULTY_MAX ) this.throwDifficulty++;
	}

  thrown()
  {
		return (this.moveCounter < 2);
	}

  decreaseInterval()
  {
		if ( this.throwInterval < 6 ) return;
		this.throwIntervalDecrease = 1;
	}

  getPercentageFree( gameGrid )
  {
    let free = 0;
		for ( let i = 0; i < gameGrid.length; i++ ) {
			for ( let j = 0; j < gameGrid[i].length; j++ ) {
				if ( !gameGrid[i][j] ) free++;
			}
		}
		return free / (gameGrid.length * gameGrid[0].length);
  }

  spacesFreeInColumn( gameGrid, column )
  {
    if ( column < 0 || column >= gameGrid.length ) return 0;
		let spaces = 0;
		for ( let i = 0; i < gameGrid[column].length; i++ ) {
			if ( ! gameGrid[column][i] ) { spaces++; }
		}
		return spaces;
  }

  getTopGamePieces( gameGrid )
  {
    let result = new Array();
    for ( let i = 0; i < gameGrid.length; i++ ) {
      for ( let j = 0; j < gameGrid[i].length; j++ ) {
        if ( gameGrid[i][j] ) {
          result.push( gameGrid[i][j] );
          break;
        }
      }
      if ( result.length <= i ) result.push( 0 );
    }
    return result;
  }

  /*
   * Returns an array of the items to be thrown.
   */
  getThrow( gameGrid )
  {
    // increase the move counter
    if( this.moveCounter >= 0 ) this.moveCounter++;

    // throw something
    if ( this.moveCounter == -1 || this.moveCounter > this.throwInterval )
    {
      // determine the number of rows possible
      let s = Math.floor( Math.random() * (this.throwDifficulty+0.9) ) + 1;

      // always start out with three lines
      if ( this.moveCounter == -1 ) { s = 3; }

      // max out at 4 possible lines
      if ( s > 4 ) s = 4;

      // if the grid is relatively full, don't spam the player
      if ( s > 2 && this.getPercentageFree( gameGrid ) < 0.3 ) s = 2;

      // make sure we are not unfair for players with high stacks in critical spaces
      let minCritSpaces = Math.max(
        this.spacesFreeInColumn(gameGrid, 2),
        this.spacesFreeInColumn(gameGrid, 3)
      );
      if ( minCritSpaces < s ) s -= 2;
      if ( s < 1 ) s = 1;

      // create an initial throw for game start
      if ( this.moveCounter == -1 ) {
        this.moveCounter = 0;
        return this.throw_dropJunk( gameGrid.length, s );
      }

      // reset the move counter
      this.moveCounter = 0;

      // if there is an interval decrease queued up, take it into account
      this.throwInterval -= this.throwIntervalDecrease;
      this.throwIntervalDecrease = 0;

      // if something is burning among the top pieces, don't throw any more burning elements
      let topPieces = this.getTopGamePieces( gameGrid );
      for ( let i = 0; i < topPieces.length; i++ ) {
        if (
          topPieces[i]
          && topPieces[i] instanceof Wood
          && topPieces[i].burning
        ) return this.throwNoBurnables(gameGrid,s);
      }

      // all throws are available
      return this.throwAll(gameGrid,s);
    }

    return null;
  }


  /*
   ***********************
   * Throwing functions. *
   ***********************
   */

  throwNoBurnables( gameGrid, s ) {
    let r = Math.random() * (10 + this.throwDifficulty) + Math.floor(this.throwDifficulty*0.5);
    if ( r < 4 ) {
      return this.throw_dropBombsOnOpenFire( this.getTopGamePieces( gameGrid ) );
    } else if ( r < 5 ) {
      return this.throw_dropMarshmallowRows( gameGrid, s );
    } else if ( r < 7 ) {
      return this.throw_dropFireRows( gameGrid.length, s );
    } else if ( r < 12 ) {
      return this.throw_dropAlternatingRows( gameGrid.length, s );
    } else {
      return this.throw_dropBombRow( gameGrid.length );
    }
  }

  throwAll( gameGrid, s ) {
    let r = Math.random() * (19 + this.throwDifficulty) + Math.floor(this.throwDifficulty*0.5);
    if ( r < 3 ) {
      return this.throw_dropJunk( gameGrid.length, s );
    } else if ( r < 13 ) {
      return this.throw_dropMarshmallowRows( gameGrid, s );
    } else if ( r < 18 ) {
      return this.throw_dropFireRows( gameGrid.length, s );
    } else if ( r < 22 ) {
      return this.throw_dropAlternatingRows( gameGrid.length, s );
    } else {
      return this.throw_dropBombRow( gameGrid.length );
    }
  }


  /*
   *****************************************
   * Helper functions for throw functions. *
   *****************************************
   */

  throw_dropJunk( rows, cols ) {
    let result = new Array();
		for ( let i = 0; i < rows; i++ ) {
			result.push( new Array() );
			for ( var j = 0; j < cols; j++ ) {
				result[i].push( this.nextPieceGenerator.generateJunkPiece() );
			}
		}
		return result;
  }

  throw_dropBombsOnOpenFire( topPieces )
	{
		var result = new Array();
		for ( let i = 0; i < topPieces.length; i++ ) {
			if ( topPieces[i] && topPieces[i] instanceof Wood && topPieces[i].burning )
			{
				result.push( [NextPieceGenerator.BOMB] );
			}
			else result.push( new Array() );
		}
		return result;
	}

  throw_dropFireOnBurnables( topPieces )
	{
		let result = new Array();
		for ( let i = 0; i < topPieces.length; i++ ) {
			if ( topPieces[i] &&
				(
          (topPieces[i] instanceof Wood && !topPieces[i].burning
          || topPieces[i] instanceof Marshmallow )
        )
      )
			{
				result.push( [NextPieceGenerator.FIRE] );
			}
			else result.push( new Array() );
		}
		return result;
	}

  throw_dropMarshmallowRows( gameGrid, rows )
	{
		var cols = gameGrid.length;

		// max out the rows for marshmallows
		if ( rows > 3 ) rows = 3;
		if ( rows > 2 && this.getPercentageFree( gameGrid ) < 0.5 ) rows = 2;
		if ( rows > 1 && this.getPercentageFree( gameGrid ) < 0.75 ) rows = 1;

		let result = new Array();
		for ( let i = 0; i < cols; i++ ) {
			result.push( new Array() );
			for ( let j = 0; j < rows; j++ ) {
				result[i].push( NextPieceGenerator.MARSHMALLOW );
			}
		}
		return result;
	}

  throw_dropFireRows( cols, rows )
	{
		// max out the rows at 2 for fire
		if ( rows > 2 ) rows = 2;

		let result = new Array();
		for ( let i = 0; i < cols; i++ ) {
			result.push( new Array() );
			for ( let j = 0; j < rows; j++ ) {
				result[i].push( NextPieceGenerator.FIRE );
			}
		}
		return result;
	}

  throw_dropBombRow( cols )
	{
		let result = new Array();
		for ( let i = 0; i < cols; i++ ) {
			result.push( [NextPieceGenerator.BOMB] );
		}
		return result;
	}

  throw_dropAlternatingRows( cols, rows )
	{
		let result = new Array();

		// used to vary the element
		let wood = false;
		if ( Math.random() < 0.5 ) wood = true;

		for ( let i = 0; i < cols; i++ ) {
			result.push( new Array() );
			for ( let j = 0; j < rows; j++ ) {
				if( wood ) {
					result[i].push( NextPieceGenerator.WOOD );
				} else {
					result[i].push( NextPieceGenerator.BRICK );
				}
				wood = !wood;
			}
			if ( !(rows % 2) ) wood = !wood;
		}
		return result;
	}

}
