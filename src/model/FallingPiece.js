export default class FallingPiece
{

	get piece1() { return { x:this.piece1X, y:this.piece1Y }; }
	get piece2() { return { x:this.piece2X, y:this.piece2Y }; }

	constructor(x1, y1, x2, y2)
	{
		this.piece1X = x1;
		this.piece1Y = y1;

		this.piece2X = x2;
		this.piece2Y = y2;
	}

	clone() {
		return new FallingPiece( this.piece1X, this.piece1Y, this.piece2X, this.piece2Y );
	}

	drop() {
		this.piece1Y += 1;
		this.piece2Y += 1;
	}

	moveLeft() {
		this.piece1X -= 1;
		this.piece2X -= 1;
	}

	moveRight() {
		this.piece1X += 1;
		this.piece2X += 1;
	}

	rotate() {
		if ( this.piece2Y == this.piece1Y ) {
			if ( this.piece2X < this.piece1X ) {
				this.piece2Y = this.piece1Y + 1;
			} else {
				this.piece2Y = this.piece1Y - 1;
			}
			this.piece2X = this.piece1X;
		} else {
			if ( this.piece2Y < this.piece1Y ) {
				this.piece2X = this.piece1X - 1;
			} else {
				this.piece2X = this.piece1X + 1;
			}
			this.piece2Y = this.piece1Y;
		}
	}

}
