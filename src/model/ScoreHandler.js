import ClearHandler from '../model/ClearHandler';

/*
 * Handles what happens when GamePieces are cleared for scored game.
 */
export default class ScoreHandler extends ClearHandler
{
  constructor()
  {
    super.constructor();

    this.score = 0;
  }

  calculateScore( clears )
  {
    let add = 0;

		for ( let i = 0; i < clears.length; i++ ) {
			let bonusMultiplier = i + 1;
			for ( let j = 0; j < clears[i].length; j++ ) {
				let clear = clears[i][j];
				add += bonusMultiplier * clear.length * 10 * clears[i].length;
			}
		}

		return add;
  }

  updateScore( clears )
  {
		this.score += this.calculateScore(clears);
		return this.score;
  }

  reser()
  {
    this.score = 0;
  }

  /*
   * Should be passed a nested array, each array representing a round of clears:
   * [
   *  ["fire","fire"],
   *  ["water","fire"],
   *  ["brick","brick","brick","brick","brick"]
   * ]
   */
  handleClears( clears ) { return this.updateScore(clears); }
}
