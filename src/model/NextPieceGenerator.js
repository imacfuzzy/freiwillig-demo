/*
 * Fetches new pieces.
 * Should be extended for single player and multiplayer functions.
 */
export default class NextPieceGenerator
{

  static get BOMB() { return "bomb"; }
  static get BRICK() { return "brick"; }
  static get WATER() { return "water"; }
  static get ICE() { return "ice"; }
  static get WOOD() { return "wood"; }
  static get FIRE() { return "fire"; }
  static get MARSHMALLOW() { return "marshmallow"; }

  constructor()
  {
    // for keeping track of what pieces have been generated
    this.pieceCounter = {
      bomb: 0,
      brick: 0,
      water: 0,
      ice: 0,
      wood: 0,
      fire: 0,
      marshmallow: 0,
    };

    this.counter = -1;
  }

  // Update the pieceCounter.
  markPieceType( gamePieceType )
  {
    this.pieceCounter[gamePieceType]++;
  }

  // Return an integer between minNum and maxNum.
  randomRange(minNum, maxNum)
  {
    return Math.round( Math.random() * (maxNum-minNum) + minNum );
  }

  getPieceType( n ) {
		if ( n < 23 ) {
			return NextPieceGenerator.BRICK;
		} else if ( n < 46 ) {
			return NextPieceGenerator.WOOD;
		} else if ( n < 69 ) {
			return NextPieceGenerator.WATER;
		} else if ( n < 79 ) {
			return NextPieceGenerator.ICE;
		} else if ( n < 90 ) {
			return NextPieceGenerator.FIRE;
		} else if ( n < 99 ) {
			return NextPieceGenerator.MARSHMALLOW;
		} else {
			return NextPieceGenerator.BOMB;
		}
	}

  generatePieceType() {
    this.counter++;

		var piece = this.getPieceType( this.randomRange(0, 101) );
		this.markPieceType( piece );
    return piece;

    /*
    switch( this.counter )
    {
      case 0: return NextPieceGenerator.BOMB;
      case 1: return NextPieceGenerator.BOMB;
      case 2: return NextPieceGenerator.BOMB;
      case 3: return NextPieceGenerator.FIRE;
      case 4: return NextPieceGenerator.BRICK;
      case 5: return NextPieceGenerator.BRICK;
      default: return NextPieceGenerator.FIRE;
    }
    */

    /*
    if( this.counter % 2 ) return NextPieceGenerator.BRICK;
    return NextPieceGenerator.WOOD;
    */
	}

  generateJunkPiece()
  {
    let range = this.randomRange(0, 92);
    if( range >= 88 )
    {
      return NextPieceGenerator.BOMB;
    }
    else if( range >= 79 )
    {
      return NextPieceGenerator.MARSHMALLOW;
    }
    else return this.getPieceType( range );
	}
}
